#ifndef DSCANVAS_H
#define DSCANVAS_H

#include <QWidget>
#include <QHash>
#include <QtOpenGL>

#include <DSShape.h>
#include <DSShapeList.h>

class DSGLWidget;
class DSMainWindow;
class Tool;

class QScrollArea;
class QHBoxLayout;
class QPaintEvent;
class QString;
class QMouseEvent;
class QKeyEvent;
class QCursor;

/*!  
 * This class represents the drawing area in the floor plan editor.  It is 
 * similar to the DSCanvas in the original Java assignment, except that in this
 * case it contains a separate DSGLWidget (a subclass of QGLWidget) to display
 * the OpenGL output.  The DSGLWidget is enclosed in a QScrollPane to allow the
 * user to scroll around.
 */
class DSCanvas : public QWidget {
    Q_OBJECT
    // the filename for this canvas
    QString m_filename;     
    // true if the canvas is untitled
    bool m_untitled;
    // true if the canvas has been modified 
    bool m_modified;
    // list of shapes displayed on this canvas
    DSShapeList* m_shapes;
    // currently selected shape
    DSShape* m_selection;
    // the editor window containing this canvas
    DSMainWindow* m_mainWindow;
    // OpenGL component
    DSGLWidget* m_drawable;
    // The scroll area containing the drawable
    QScrollArea* m_scrollArea;
    // The layout used for the canvas
    QHBoxLayout* m_layout;
    // The map of tool name to tool instance
    QHash<QString, Tool*>* m_tools;
    // The currently selected tool
    Tool* m_currentTool;
public:
    /*! 
     * The constructor is given the reference to a parent frame.  It is expected
     * that after the instance is constructed there will be a call to openFile()
     * or newFile()
     */
    DSCanvas(DSMainWindow& mainWindow);
    /*! 
     * Destructor.  Frees up all memory occupied by this widget
     */
    ~DSCanvas();
    /*! 
     * Creates a new file.  Keeps a static counter of all newly created files to
     * set the window title to `untitled x'.  
     */
    void newFile();
    /*!
     * Opens an existing file.  Sets the window title to the name of the opened
     * file
     */
    void openFile(const QString& filename);
    /*!
     * If the current file is untitled, calls saveAs().  Otherwise calls
     * saveFile().  Returns true if the file has been successfully saved.
     */
    bool save();
    /*!
     * Prompts the user for a filename to save to and calls saveFile().  Returns
     * true if the file has been successfully saved.
     */
    bool saveAs();
    /*!
     * Performs the actual save operation.  Sets the modified flag to false.  
     * Returns true if file has been successfully saved.
     */
    bool saveFile(const QString& filename);
    /*!
     * If the file has been modified, opens a confirmation dialog allowing the
     * user to save their changes.  Returns true if the file has been
     * successfully closed, false otherwise.
     */
    bool closeFile();
    /*!
     * Forces the canvas to repaint itself
     */
    void repaint();
    /*!
     * Return the shapes on this canvas
     */
    DSShapeList& getShapes();
    /*!
     * Set the currently selected shape for this canvas
     */
    void select(DSShape* r);
    /*! 
     * Return the selected shape.  Returns 0 if no shape is currently selected
     */    
    DSShape* getSelection();
    /*!
     * Get the position of the horizontal scroll bar
     */
    int getXOffset();
    /*!
     * Get the position of the vertical scroll bar
     */
    int getYOffset();
    /*! 
     * Set the modified flag to true - indicates that the canvas has been 
     * changed
     */
    void modified();
    /*!
     * Returns the texture handle given the filename. 
     * If the texture already exists in memory the handle is returned.
     * If no such texture exists one is loaded from the file.
     */
    GLuint getTexture (QString& texture);
    /*!
     * Returns the tool currently being used by this canvas, 0 if none
     */
    Tool* getCurrentTool(); 
public slots:
    /*!
     * This slot changes the currently selected tool given the name of the tool.
     * It will also update the mouse cursor
     */
    void toolChanged(const QString& name);
protected:
    /*!
     * This slot closes the window containing the canvas.  If the canvas has
     * been modified since the last save, then it will prompt the user
     */
    void closeEvent(QCloseEvent* event);
private slots:
    /*!
     * Handle a mouse movement event.  Will forward it to the currently selected
     * tool
     */
    void handleMouseMoveEvent(QMouseEvent* evt);
    /*!
     * Handle a mouse button press.  Will forward it to the currently selected
     * tool
     */
    void handleMousePressEvent(QMouseEvent* evt);
    /*!
     * Handle a mouse button release.  Will forward it to the currently selected
     * tool
     */
    void handleMouseReleaseEvent(QMouseEvent* evt);
    /*!
     * Handle a keyboard key press event.  Will forward it to the currently 
     * selected tool
     */
    void handleKeyPressEvent(QKeyEvent* evt);
    /*!
     * Handle a keyboard key release event.  Will forward it to the currently 
     * selected tool
     */
    void handleKeyReleaseEvent(QKeyEvent* evt);
};

#endif // #ifndef DSCANVAS_H
