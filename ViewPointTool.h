#ifndef VIEWPOINTTOOL_H
#define VIEWPOINTTOOL_H

#include <Tool.h>

class DSViewPoint;
class DSMainWindow;
class QMouseEvent;

/*!
 * Viewpoint tool for graphical editor: implements the tool for interactive
 * creation of viewpoint shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class ViewPointTool : public Tool {
public:
    /*!
     * ViewPointTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    ViewPointTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~ViewPointTool();
    /*!
     * Deletes the viewpoint currently being drawn
     */
    void cancel();
protected:
    /*!
     * The viewpoint currently being created.
     */
    DSViewPoint* m_p;

    /*!
     * Handles mouse click events - if a viewpoint is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new viewpoint.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef VIEWPOINTTOOL_H
