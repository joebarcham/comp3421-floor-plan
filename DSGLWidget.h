#ifndef DSGLWIDGET_H
#define DSGLWIDGET_H

#include <QGLWidget>
#include <QHash>

class DSCanvas;
class QMouseEvent;
class QKeyEvent;
class QSize;

/*!  
 * This class is the GL widget onto which everything will be drawn.  It
 * implements some functions from the DSCanvas class in the original Java
 * assignment.  It emits signals when the mouse is clicked or moved, or when
 * a key is pressed.
 */
class DSGLWidget : public QGLWidget {
    Q_OBJECT
public:
    /*!
     * Construct a new instance of this class
     */
    DSGLWidget(DSCanvas* parent);
    /*!
     * Free all memory being used by this object
     */
    ~DSGLWidget();
    /*!
     * Returns the preferred size of this widget
     */
    QSize sizeHint() const;
    /*!
     * Binds the texture given the name of the image file.  Returns a handle
     * for the texture, which can then be used for drawing objects
     */
    GLuint getTexture(QString& texture);
signals:
    /*!
     * This signal is emitted when the mouse button is pressed 
     */
    void mousePressed(QMouseEvent* event);
    /*! 
     * This signal is emitted when the mouse is moved while a button is being
     * held down
     */
    void mouseMoved(QMouseEvent* event);
    /*!
     * This signal is emitted when the mouse button is released
     */
    void mouseReleased(QMouseEvent* event);
    /*! 
     * This signal is emitted when a key is pressed 
     */
    void keyPressed(QKeyEvent* event);
    /*!
     * This signal is emitted when a key is released 
     */
    void keyReleased(QKeyEvent* event);
protected:
    /*!
     * Performs the initialisation of the OpenGL drawing space.  Fills
     * everything with black
     */
    void initialiseGL();    
    /*!
     * Repaints the widget.  Retrieves the list of shapes from the parent canvas
     * and paints all of them.  Then paints over the selected shape if there is
     * one
     */
    void paintGL();
    /*!
     * Called to resize the OpenGL drawing area
     */    
    void resizeGL(int width, int height);
    /*!
     * Called when a mouse button is pressed
     */
    void mousePressEvent(QMouseEvent* event);
    /*!
     * Called when the mouse is moved
     */
    void mouseMoveEvent(QMouseEvent* event);
    /*!
     * Called when the mouse button is released
     */
    void mouseReleaseEvent(QMouseEvent* event);
    /*!
     * Called when a keyboard key is pressed
     */
    void keyPressEvent(QKeyEvent* event);
    /*!
     * Called when a keyboard key is released
     */
    void keyReleaseEvent(QKeyEvent* event);   
    /*! 
     * Resets the used textures tally
     */
    void resetTextureCount();
    /*!
     * Removes textures not used since the last call to resetTextureCount().
     */
    void removeUnsedTextures();
private:
    DSCanvas* m_parent;
    // stores the handles for all loaded textures
    QHash<QString, GLuint> *m_textures;     
    // used to keep a tally of which textures are being used
    QHash<QString, GLuint> *m_textureTally;
    static const int WIDTH = 1000;    
    static const int HEIGHT = 1000;
    static const int VIEWPORT_WIDTH = 500;
    static const int VIEWPORT_HEIGHT = 300;
};
#endif // #ifndef DSGLWIDGET_H
