//------------------------------------------------------------------------------
// ReshapeTool
//------------------------------------------------------------------------------
// Reshape tool for image map editor
//------------------------------------------------------------------------------

#include <ReshapeTool.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>

#include <QCursor>
#include <QString>
#include <QMouseEvent>


//------------------------------------------------------------------------------
// ReshapeTool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, point to NULL, the window using parent's constructor.
//------------------------------------------------------------------------------

ReshapeTool::ReshapeTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    :Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_shapeSelected = 0;          // init shape pointer to NULL
    m_pointSelected = 0;

    // delete default cursor
    delete m_cursor;

    // create new name, message and cursor
    m_name    = new QString("Reshape");
    m_message = new QString("Click and drag a point to move it");
    m_cursor  = new QCursor(Qt::SizeAllCursor);
}


//------------------------------------------------------------------------------
// ReshapeTool Destructor
//------------------------------------------------------------------------------
// If a point was allocated, delete it. Shape pointer never gets allocated, no
// need to delete.
//------------------------------------------------------------------------------

ReshapeTool::~ReshapeTool(){
    if(m_pointSelected) {
        delete m_pointSelected;
    }
}


//------------------------------------------------------------------------------
// mousePressed
//------------------------------------------------------------------------------
// On button down, highlight the region, and display a message.
//------------------------------------------------------------------------------
void ReshapeTool::mousePressed(QMouseEvent* evt) {
    const char* methName = "ReshapeTool::mousePressed(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    // ask the canvas to find a shape under the cursor
    m_shapeSelected = m_canvas->getShapes().findShape(getPoint(evt)); 
    if (!m_shapeSelected)
        return;  // there was no shape where user clicked

    m_pointSelected = new QPoint(m_shapeSelected->selectPoint(getPoint(evt)));

    if (!m_pointSelected || m_pointSelected->x() == -1 || m_pointSelected->y() == -1)
        return;

    m_canvas->select(m_shapeSelected); // select shape
    m_canvas->repaint();               // refresh canvas
}


//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// Drag selected object around
//------------------------------------------------------------------------------
void ReshapeTool::mouseDragged(QMouseEvent* evt) {
    const char* methName = "ReshapeTool::mouseDragged():";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_shapeSelected) {
        if (debug)
            printf("%s no selected shape\n", methName);

        return;
    }

    QPoint newPos = gridPoint(evt);// get point under cursor
    // move shape to point under cursor
    m_shapeSelected->setPoint(newPos);

    m_canvas->modified();          // tell the canvas the file has been modified
    m_canvas->repaint();
}

void ReshapeTool::mouseReleased(QMouseEvent* evt) {
    if(m_pointSelected) {
        delete m_pointSelected;
        m_pointSelected = 0;
    }
}