#ifndef WALLTOOL_H
#define WALLTOOL_H

#include <Tool.h>

class DSWall;
class DSMainWindow;
class QMouseEvent;

/*!
 * Wall tool for graphical editor: implements the tool for interactive
 * creation of wall shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class WallTool : public Tool {
public:
    /*!
     * WallTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    WallTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~WallTool();
    /*!
     * Deletes the wall currently being drawn
     */
    void cancel();
protected:
    /*!
     * The wall currently being created.
     */
    DSWall* m_p;

    /*!
     * Handles mouse click events - if a wall is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new wall.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef WALLTOOL_H
