#ifndef TEXTURECHOICE_H
#define TEXTURECHOICE_H

#include <QWidget>

class QLabel;
class DSMainWindow;
class QString;
class QComboBox;
class QLineEdit;
class QPushButton;
class QColorDialog;
class QDoubleValidator;

/*!
 * TextureChoice allows the colour, texture, scale, and height to be selected.
 * The selected values can be accessed through the accessor methods.
 */
class TextureChoice : public QWidget {
    Q_OBJECT
    QLabel*           m_textureLabel;
    QComboBox*        m_textureList;
    QLabel*           m_scaleLabel;
    QLineEdit*        m_scaleValue;
    QDoubleValidator* m_scaleValidator;
    QLabel*           m_colourLabel;
    QPushButton*      m_colourButton;
    QColor            m_currentColour;
    QColorDialog*     m_colourDialog;
public:
    /*! 
     * Construct a new instance of this object 
     */
    TextureChoice(QWidget* parent = 0);
    /*!
     * Free all memory being occupied by this object
     */
    ~TextureChoice();
    /*!
     * Get the name of the currently selected texture
     */
    QString getTexture() const; 
    /*!
     * Set the currently selected texture given its' name
     */
    void setTexture(QString& texture);
    /*!
     * Get the currently selected colour
     */
    QColor& getColour();
    /*!
     * Set the currently selected colour
     */
    void setColour(QColor colour);  
    /*!
     * Get the currently selected scale
     */
    double getScale() const;
    /*!
     * Set the currently selected scale
     */
    void setScale(double scale);
private slots:
    /*
     * This slot brings up a dialog allowing a colour to be picked
     */
    void showColourDialog();
private:
    // sets the background colour of a button
    static void setBackgroundColour(QPushButton& button, QColor& colour);
};

#endif //  #ifndef TEXTURECHOICE_H
