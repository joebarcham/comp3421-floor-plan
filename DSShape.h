#ifndef DSSHAPE_H
#define DSSHAPE_H

#include <QColor>
#include <Exceptions.h>

class QString;
class StringTokenizer;
class QPoint;
class Point2D;
class DSCanvas;

/*! 
 * This class represents an abstract geometric figure.  It represents the common 
 * functionality between shapes for the map editor.  Inherit from this class when
 * implementing concrete shape subclasses.
 */
class DSShape {
public:
    /*!
     * Abstract constructor.  Call this constructor from subclass constructos
     * for common shape initialisation
     */
    DSShape();
    /*!
     * Copy constructor
     */
    DSShape(DSShape& s);
    /*!
     * This destructor will be automatically called prior to any subclass' 
     * destructor to free any memory occupied by this object.
     */
    virtual ~DSShape();
    /*! 
     * Set the fill colour 
     */
    void setFillColour(QColor fill);
    /*!
     * Get the fill colour 
     */
    QColor& getFillColour();
    /*!
     * Set the texture
     */
    void setTexture(QString texture);
    /*!
     * Get the texture
     */
    QString& getTexture();
    /*! 
     * Set the texture scale 
     */
    void setScale(double scale);
    /*!
     * Get the texture scale 
     */
    double getScale();  
    /*!
     * sets the 3D Height of the object (or width for roads)
     */
    void setExtra1(double extra);
    void setExtra2(double extra);
    /*!
     * Get the 3D Height of the object (or width for roads)
     */
    double getExtra1();
    double getExtra2();
    /*!
     * Get the name of the shape 
     */
    QString getName();
    /*! 
     * Get the current canvas 
     */
    DSCanvas* getCanvas();
    /*! 
     * Set the canvas for this shape 
     */
    void setCanvas(DSCanvas& canvas);
    /*! 
     * This draws a point given the co-ordinates 
     */
    void drawPoint(double x, double y);    
    /*!
     * Creates a shape from a string describing it.  This must consist of a
     * sequence of space separated fields.  The first field is the name of a
     * class that is a child of DSShape.  The next three are the colour, RGB format. 
     * The next is the name of the texture.  Further fields will be interpreted
     * by the child's fromTokens method.  Returns the clickable region
     * described by the string.  Throws a MalformedShapeException if there is
     * problem parsing
     */
    static DSShape* create(QString& s) throw(MalformedShapeException);

    /* 
     * Pure abstract functions to be implemented by subclasses
     */

    /*! 
     * Paints the shape on the screen 
     */
    virtual void paint() = 0;
    /*! 
     * Paints a selected shape on the screen.  If a shape is selected, paint
     * is called first, then paintSelect
     */
    virtual void paintSelect() = 0;        
    /*!
     * Determines whether a particular point is inside the shape.  Returns
     * true if so, false otherwise
     */
    virtual bool contains(int x, int y) = 0;
    /*!
     * Translates the shape by a specified amount 
     */
    virtual void translate(int deltaX, int deltaY) = 0;
    /*! 
     * Rotates the shape around a specified point through a specified angle 
     */
    virtual void rotate(const Point2D& fixed, double angle) = 0;

    virtual void scale(double xScale, double yScale) = 0;
    /*! 
     * Returns the centre of the shape 
     */
    virtual Point2D centre() = 0;
    /*!
     * This method adds a control point to a shape.  What the control point
     * means depends on the shape.  For a polygon, the control points are the
     * corners.  For a rectangle, they are two diagonally opposite corners and
     * so on for other sorts of shapes.  This method can be used to 
     * interactively create a shape.
     */
    virtual void addPoint(QPoint p) = 0;
    /*! 
     * This method selects the control point to be modified by the setPoint
     * method.  This would typically be the closest one to that specified in
     * the parameters.  This method can be used in conjunction with setPoint to
     * interactively modify a shape
     */
    virtual QPoint selectPoint(const QPoint& p) = 0;
    /*! 
     * This method gives the selected control point a new value.  The selected
     * point is the one specified by the previous call to selectPoint, or the
     * one most recently added.  Returns 0 if there is no selected point.
     */
    virtual void setPoint(QPoint p) = 0;
    /*! 
     * Returns the string representation of this object.  It must be suitable
     * to be used in the create method to recreate a copy of this object
     */
    virtual QString toString();
    /*!
     * Creates an exact copy of this DSShape 
     */
    virtual DSShape* clone() = 0;     
protected:
    /*!
     * The name of this shape.  Subclasses should re-initialise this to the 
     * subclass name.
     */
    QString m_name;
    /*!
     * The canvas that the shape is associated with.  Can be 0.
     */
    DSCanvas* m_canvas;
    /*!
     * The selected texture for this object
     */
    QString m_texture;
    /*!
     * The scaling of the texture for this object
     */
    double m_scale;
    /*!
     * The fill colour for this shape, if no texture is used
     */
    QColor m_fill;
    /*!
     * The height of this object in a 3D world (or width for roads)
     */
    double m_extra1;
    double m_extra2;
    /*! 
     * This must be implemented by child classes to parse the remaining fields
     * of a string argument to create
     */
    virtual void fromTokens(StringTokenizer& st) throw (MalformedShapeException) = 0;
    /*!
     * Sets the current GL drawing colour to the current fill colour 
     */
    void setGLColour();
private:
    static const int SIZE = 2;
};

#endif // #ifndef DSSHAPE_H
