#include <DSLight.h>
#include <Edge2D.h>
#include <Matrix2D.h>
#include <Point2D.h>
#include <StringTokenizer.h>
#include <Exceptions.h>

#include <QPolygon>
#include <QPoint>
#include <QVector>
#include <QRegion>
#include <QtOpenGL>

#include "DSCanvas.h"

#include <iostream>
#include <math.h>

using namespace std;

DSLight::DSLight() : DSShape() {
    m_Position = new Point2D();
    initialised = false;
    radius = 6.0;

    m_name = "Light";
}

DSLight::DSLight(DSLight& p) : DSShape(p) {
    m_Position = new Point2D(*p.m_Position);
    m_fill = p.m_fill;
}

DSLight::~DSLight() {
    delete m_Position;
}

void DSLight::paint() {
    setGLColour();
    glDisable(GL_TEXTURE_2D);
    glLineWidth(1.0);

    if (initialised) {
        glBegin(GL_POLYGON);  //draw the light 
            double angle;
            for (int i = 0; i < line_count ; ++i) {
                angle = i*2*M_PI/line_count;
                glVertex2d(m_Position->x() + (cos(angle) * radius),
                             m_Position->y() + (sin(angle) * radius));
            }
        glEnd(); 
    }
}

void DSLight::paintSelect() {
    drawPoint(m_Position->x(), m_Position->y());
}

bool DSLight::contains(int x, int y) {
    return sqr(radius) >= sqr(x - m_Position->x()) + sqr(y - m_Position->y());
}

void DSLight::rotate(const Point2D& fixed, double angle) {
    // You want me to what?? Its just a point, rotating it makes no sense!
}

void DSLight::translate(int deltax, int deltay) {
    const Matrix2D matrix = Matrix2D(1, 0, deltax, 0, 1, deltay);
    apply(matrix);
}

void DSLight::scale(double xFactor, double yFactor) {
    radius *= (xFactor + yFactor) / 2;
}

void DSLight::addPoint(Point2D p) {
    if (initialised == false) {
        setPoint(p);
        initialised = true;
    }
}

void DSLight::addPoint(QPoint p) {
    addPoint(Point2D(p));
}

void DSLight::setPoint(QPoint p) {
    setPoint(Point2D(p));
}

void DSLight::setPoint(Point2D p) {
    delete m_Position;
    m_Position = new Point2D(p);
}

QPoint DSLight::selectPoint(const QPoint& p) {
    return m_Position->toPoint();
}

void DSLight::removePoint() {
    delete this;
}

Point2D DSLight::centre() {
    return *m_Position;
}

void DSLight::apply(const Matrix2D& t){
    m_Position = new Point2D(t.apply(*m_Position));
}

QString DSLight::toString() {
    return QString(DSShape::toString() + " 1 " + m_Position->toString());
}

void DSLight::fromTokens(StringTokenizer& st) throw(MalformedShapeException) {
    try {
        bool ok;
        int n = (st.nextToken()).toInt(&ok);  // how many points
        if ( !ok ) {
                throw NumberFormatException();
        }
        double x = (st.nextToken()).toDouble(&ok);
        if ( !ok ) {
            throw NumberFormatException();
        }
        double y = (st.nextToken()).toDouble(&ok);
        if ( !ok ) {
            throw NumberFormatException();
        }
        addPoint(Point2D(x, y));
    }
    catch (NoSuchElementException e) {
        throw (MalformedShapeException("NoSuchElementException " +
                    e.getMessage()));
    }
    catch (NumberFormatException e) {
        throw (MalformedShapeException("NumberFormatException " + 
                    e.getMessage()));
    }
}

double DSLight::sqr(double x) {
    return x*x;
}

DSShape* DSLight::clone() {
    return new DSLight(*this);
}
