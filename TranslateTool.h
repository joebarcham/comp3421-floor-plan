#ifndef TRANSLATETOOL_H
#define TRANSLATETOOL_H

#include <Tool.h>
#include <QPoint>

class DSMainWindow;
class DSCanvas;
class DSShape;
class QCursor;
class QMouseEvent;

/*!
 * Translation tool for graphical editor: selects the appropriate shape when it
 * is clicked, moves this shape around when the mouse is being dragged.
 *  Inherits a lot of common functions from Tool.
 */
class TranslateTool : public Tool {
public:
    /*!
     * TranslateTool's constructor, needs the window and the canvas that this 
     * tool is operating on.
     */
    TranslateTool(DSMainWindow &mainWindow, DSCanvas* canvas);
    ~TranslateTool();

protected:
    /*!
     * Handles mouse press events (same as mouse click). When mouse button is 
     * pressed, this tool selects it.
     */
    void mousePressed(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events. If the user drags the mouse while a shape
     * is selected, the shape moves to follow the cursor.
     */
    void mouseDragged(QMouseEvent* evt);

private:
    DSShape* m_shapeSelected; // Pointer to the currently selected shape.
    QPoint   m_lastPosition;  // The point where the shape was last
};

#endif // #ifndef TRANSLATETOOL_H

