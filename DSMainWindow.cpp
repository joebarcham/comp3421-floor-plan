#include <DSMainWindow.h>
#include <DSCanvas.h>
#include <DSShape.h>
#include <Tool.h>
#include <ToolChoice.h>
#include <TextureChoice.h>
#include <PropertiesDialog.h>

#include <QtGui>

DSMainWindow::DSMainWindow() {
    m_currentTool = 0;
    m_clipboard = 0;
    
    // workspace init
    m_workspace = new QWorkspace();
    setCentralWidget(m_workspace);
    m_windowMapper = new QSignalMapper(this);    
    connect(m_windowMapper, SIGNAL(mapped(QWidget*)), m_workspace,
                SLOT(setActiveWindow(QWidget*)));
    
    // GUI init
    createActions();
    createMenus();
    createToolbars();
    setWindowTitle("DSEdit");    
    m_properties = new PropertiesDialog(this);
    resize(WINDOW_WIDTH, WINDOW_HEIGHT);
    setWindowIcon (QIcon(":/images/icon.png"));
    statusBar()->showMessage("Ready");
}

DSMainWindow::~DSMainWindow() {
    // TODO:  clean up
    delete m_workspace;
    delete m_windowMapper;

    // destroy main bar and its widgets
    // delete m_mainBar; for some reason it does not like destroying the main bar
    delete m_toolChoice;
    delete m_textureChoice;

    // destroy actions 
    delete m_fileMenu;
    delete m_newAction;
    delete m_openAction;
    delete m_saveAction;
    delete m_saveAsAction;
    delete m_closeAction;
    delete m_exitAction;

    delete m_editMenu;
    delete m_cutAction;
    delete m_copyAction;
    delete m_pasteAction;
    delete m_toFrontAction;
    delete m_toBackAction;
    delete m_propertiesAction;

    delete m_helpMenu;
    delete m_aboutAction;

    // destroy tool bars
    delete m_fileToolbar;
    delete m_editToolbar;
    delete m_drawToolbar;
}

DSCanvas* DSMainWindow::getCurrentCanvas() {
    return qobject_cast<DSCanvas*>(m_workspace->activeWindow());
}

void DSMainWindow::assignProperties(DSShape& s) {
    QColor& colour = m_textureChoice->getColour(); 
    s.setFillColour(colour);
    QString texture = m_textureChoice->getTexture();
    s.setTexture(texture);
    s.setScale(m_textureChoice->getScale());
    s.setExtra1(m_toolChoice->getExtra1());
    s.setExtra2(m_toolChoice->getExtra2());
}

int DSMainWindow::getGridSize() {
    return m_toolChoice->getGridSize();
}

double DSMainWindow::getExtra1() {
    return m_toolChoice->getExtra1();
}

double DSMainWindow::getExtra2() {
    return m_toolChoice->getExtra2();
}

void DSMainWindow::closeEvent(QCloseEvent* event) {
    const char* methName = "DSMainWindow::closeEvent():";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    m_workspace->closeAllWindows();
    if (getCurrentCanvas() != 0) { // still open
        event->ignore();
    } else {
        event->accept();
    }
}

void DSMainWindow::createActions() {
    const char* methName = "DSMainWindow::createActions():";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    m_newAction = new QAction(QIcon(":/images/new.png"), "&New", this);
    m_newAction->setStatusTip("Create a new file");
    m_newAction->setShortcut(QString("Ctrl+N"));
    connect(m_newAction, SIGNAL(triggered()), this, SLOT(newFile())); 
    
    m_openAction = new QAction(QIcon(":/images/open.png"), 
            "&Open", this);
    m_openAction->setStatusTip("Open an existing file");
    m_openAction->setShortcut(QString("Ctrl+O"));
    connect(m_openAction, SIGNAL(triggered()), this, SLOT(openFile()));
    
    m_saveAction = new QAction(QIcon(":/images/save.png"), "&Save", this);
    m_saveAction->setStatusTip("Save changes to the current file");
    m_saveAction->setShortcut(QString("Ctrl+S"));
    connect(m_saveAction, SIGNAL(triggered()), this, SLOT(saveFile()));
    
    m_saveAsAction = new QAction("Save &as", this);
    m_saveAsAction->setStatusTip("Save as another file");
    m_saveAsAction->setShortcut(QString("Ctrl+Shift+S"));
    connect(m_saveAsAction, SIGNAL(triggered()), this, SLOT(saveAs()));
    
    m_closeAction = new QAction("&Close", this);
    m_closeAction->setStatusTip("Close the currently selected file");
    m_closeAction->setShortcut(QString("Ctrl+W"));
    connect(m_closeAction, SIGNAL(triggered()), this, SLOT(closeFile()));
    
    m_exitAction = new QAction("E&xit", this);
    m_exitAction->setStatusTip("Exit the application");
    m_exitAction->setShortcut(QString("Ctrl+Q"));
    connect(m_exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    m_cutAction = new QAction(QIcon(":/images/cut.png"), "C&ut", this);
    m_cutAction->setStatusTip("Move currently selected item to buffer");
    m_cutAction->setShortcut(QString("Ctrl+X"));
    connect(m_cutAction, SIGNAL(triggered()), this, SLOT(cut()));
    
    m_copyAction = new QAction(QIcon(":/images/copy.png"), "&Copy", this);
    m_copyAction->setStatusTip("Copy currently selected item to buffer");
    m_copyAction->setShortcut(QString("Ctrl+C"));
    connect(m_copyAction, SIGNAL(triggered()), this, SLOT(copy()));
    
    m_pasteAction = new QAction(QIcon(":/images/paste.png"), 
            "&Paste", this);    
    m_pasteAction->setStatusTip("Move in item from buffer");        
    m_pasteAction->setShortcut(QString("Ctrl+V"));
    connect(m_pasteAction, SIGNAL(triggered()), this, SLOT(paste()));
    
    m_toFrontAction = new QAction("Move to &Front", this);
    m_toFrontAction->setStatusTip(
            "Move the currently selected item in front of all others");
    m_toFrontAction->setShortcut(QString("Ctrl+F"));
    connect(m_toFrontAction, SIGNAL(triggered()), this, SLOT(toFront()));
    
    m_toBackAction = new QAction("Move to &Back", this);
    m_toBackAction->setStatusTip(
            "Move the currently selected item in behind all others");
    m_toBackAction->setShortcut(QString("Ctrl+B"));
    connect(m_toBackAction, SIGNAL(triggered()), this, SLOT(toBack()));
    
    m_propertiesAction = new QAction("&Properties", this);
    m_propertiesAction->setStatusTip(
            "Display properties of the currently selected item");
    m_propertiesAction->setShortcut(QString("Ctrl+P"));
    
    m_aboutAction = new QAction("&About", this);
    m_aboutAction->setStatusTip("About this program");
    m_aboutAction->setShortcut(Qt::Key_F1);
    connect(m_aboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));
    if (debug) {
        printf("%s exiting\n", methName);
    }
}

void DSMainWindow::createMenus() {
    const char* methName = "DSMainWindow::createMenus():";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    
    m_fileMenu = menuBar()->addMenu("&File");
    m_fileMenu->addAction(m_newAction);
    m_fileMenu->addAction(m_openAction);
    m_fileMenu->addAction(m_saveAction);
    m_fileMenu->addAction(m_saveAsAction);
    m_fileMenu->addAction(m_closeAction);
    m_fileMenu->addAction(m_exitAction);
    
    m_editMenu = menuBar()->addMenu("&Edit");
    m_editMenu->addAction(m_cutAction);
    m_editMenu->addAction(m_copyAction);
    m_editMenu->addAction(m_pasteAction);
    m_editMenu->addAction(m_pasteAction);
    m_editMenu->addAction(m_toFrontAction);
    m_editMenu->addAction(m_toBackAction);
    m_editMenu->addAction(m_propertiesAction);
    connect(m_propertiesAction, SIGNAL(triggered()), this, 
            SLOT(showPropertiesDialog()));
    
    m_helpMenu = menuBar()->addMenu("&Help");
    m_helpMenu->addAction(m_aboutAction);
    if (debug) {
        printf("%s exiting\n", methName);
    }
}

void DSMainWindow::createToolbars() {
    const char* methName = "DSMainWindow::createToolbars():";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    m_fileToolbar = addToolBar("File");
    m_fileToolbar->addAction(m_newAction);
    m_fileToolbar->addAction(m_openAction);
    m_fileToolbar->addAction(m_saveAction);

    m_editToolbar = addToolBar("Edit");
    m_editToolbar->addAction(m_cutAction);
    m_editToolbar->addAction(m_copyAction);
    m_editToolbar->addAction(m_pasteAction);

    addToolBarBreak();
    m_drawToolbar = addToolBar("Draw");

    m_mainBar = createMainBar();
    m_drawToolbar->addWidget(m_mainBar);
    if (debug) {
        printf("%s exiting\n", methName);
    }
}

QWidget* DSMainWindow::createMainBar() {
    const char* methName = "DSMainWindow::createMainBar():";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    QWidget* result = new QWidget();
    m_toolChoice = new ToolChoice(*this, result);
    m_textureChoice = new TextureChoice(result);
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addStretch(1);
    layout->addWidget(m_toolChoice);
    layout->addWidget(m_textureChoice);
    layout->addStretch(1);
    result->setLayout(layout);
    if (debug) {
        printf("%s exiting\n", methName);
    }
    return result;
}

void DSMainWindow::toolChanged(const Tool& tool) {
    bool debug = false;
    const char* methName = "DSMainWindow::toolChangedSlot():";
    if (debug)
        printf("%s called\n", methName);
    emit toolChanged(tool.getName());
    statusBar()->showMessage(tool.getMessage());
}

void DSMainWindow::newFile() {
    DSCanvas* child = createChildWindow();
    child->newFile();
    child->show();
}

void DSMainWindow::openFile() {
    QString str = QFileDialog::getOpenFileName(this, "Choose a file to open",
            ".", "Floor Plans (*.fpe);; All Files (*)");

    if (str.isEmpty())
        return;

    DSCanvas* child = createChildWindow();
    child->openFile(str);
    child->show();
}

void DSMainWindow::openFile(char *fileName){
    if (!fileName){  // null filename
        newFile();
        return;
    }

    QString file(fileName);
    QDir currentDir;

    if (!currentDir.exists(file)){
        QMessageBox::critical(this, "File not found", 
                QString("File not found - %1").arg(QString(fileName)), 
                QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
        newFile();
    } else {
        DSCanvas* child = createChildWindow();
        child->openFile(file);
        child->show();
    }
}

void DSMainWindow::saveFile() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    canvas->save();
}

void DSMainWindow::saveAs() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    canvas->saveAs();
}

void DSMainWindow::closeFile() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    if (canvas->close()) {
        delete canvas;
    }
}

void DSMainWindow::cut() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    // ensure the current tool isn't doing anything
    Tool* tool = canvas->getCurrentTool();
    if (tool)
        tool->cancel();
    DSShape* selected = canvas->getSelection();
    if (selected == 0) {
        return;
    }
    m_clipboard = selected;
    DSShapeList& shapes = canvas->getShapes();
    shapes.remove(selected);
    canvas->select(0);
    canvas->repaint();
    canvas->modified();
}

void DSMainWindow::copy() {
    const char* methName = "DSMainWindow::copy():";
    bool debug = false;
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    DSShape* shape = canvas->getSelection();
    if (shape == 0) {
        return;
    }
    m_clipboard = shape->clone();
    if (debug) {
        printf("%s original: %x copied: %x\n", methName, 
        (unsigned int) &shape, (unsigned int) &m_clipboard);
    }
}

void DSMainWindow::paste() {
    if (m_clipboard == 0) {
        return;
    }
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    DSShapeList& shapes = canvas->getShapes();
    DSShape* cloned = m_clipboard->clone();
    cloned->setCanvas(*canvas);
    shapes.add(cloned);
    canvas->select(cloned);
    canvas->repaint();
    canvas->modified();
}

void DSMainWindow::toFront() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == NULL) return;

    DSShapeList& shapeList = canvas->getShapes();
    DSShape* selected = canvas->getSelection();
    if (shapeList.size() <= 1 || selected == NULL) return;

    int index = shapeList.getIndex(selected);
    if (index == -1) return;

    shapeList.remove(selected);
    shapeList.insert(shapeList.size(), selected);
    canvas->repaint();
    canvas->modified();
}

void DSMainWindow::toBack() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == NULL) return;

    DSShapeList& shapeList = canvas->getShapes();
    DSShape* selected = canvas->getSelection();
    if (shapeList.size() <= 1 || selected == NULL) return;

    int index = shapeList.getIndex(selected);
    if (index == -1) return;

    shapeList.remove(selected);
    shapeList.insert(0, selected);
    canvas->repaint();
    canvas->modified();
}

DSCanvas* DSMainWindow::createChildWindow() {
    DSCanvas* child = new DSCanvas(*this);
    m_workspace->addWindow(child);
    child->toolChanged(m_toolChoice->getTool());
    connect(this, SIGNAL(toolChanged(const QString&)),
            child, SLOT(toolChanged(const QString&)));
    return child;
}

void DSMainWindow::showPropertiesDialog() {
    DSCanvas* canvas = getCurrentCanvas();
    if (canvas == 0) {
        return;
    }
    DSShape* shape = canvas->getSelection();
    if (shape == 0) {
        return;
    }
    m_properties->setShape(*shape);
    m_properties->exec();
}

void DSMainWindow::showAbout() {
    QMessageBox::about(this, "About the DSed C++ Port",
        "DSed C++ Port: Comp3421/9415 05s2\n\n"
        "Brought to you by:\n"
        "Michael Penkov\n"
        "Glen Kelly\n"
        "Alex Kuptsov\n"
        "Dan Mansfield\n"
        "John Spann\n\n"
        "Completed by: Joseph Barcham\n"
        "CSE user: jrba250\n"
        "COMP3421 12s2");
}
