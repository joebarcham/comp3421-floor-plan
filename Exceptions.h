#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <QString>
#include <string>

using std::string;

/*!
 * General exception class for the DSEdit application.  More specific exception
 * classes should inherit from this
 */
class DSEditException {
public:
    /*!
     * Create an exception with an empty message
     */
    DSEditException() : m_message("") { }
    /*!
     * Create an exception with the specified message
     */
    DSEditException(string s) : m_message(s) { }
    /*!
     * Create an exception with the specified message
     */
    DSEditException(QString s) : m_message(s.toStdString()) { }
    /*!
     * Get the message contained by this exception
     */
    string getMessage( ) const { return m_message; }
protected:
    string m_message;
};

/*!
 * This exception is thrown when a string describing 
 * a DSShape cannot be parsed
 */
class MalformedShapeException : public DSEditException { 
public:
    MalformedShapeException() : DSEditException() { };
    MalformedShapeException(string s) : DSEditException(s) { };
    MalformedShapeException(QString s) : DSEditException(s) { };
};
/*!
 * This exception is thrown when the editor cannot find the required class to
 * load.  This will occur when features that have not been implemented yet have
 * been invoked
 */
class ClassNotFoundException : public DSEditException {
public:
    ClassNotFoundException() : DSEditException() { };
    ClassNotFoundException(string s) : DSEditException(s) { };
    ClassNotFoundException(QString s) : DSEditException(s) { };
};
/*!
 * This exception is thrown when a StringTokenizer 
 * has no more tokens to parse and recieves a nextToken() call
 */
class NoSuchElementException : public DSEditException { 
public:
    NoSuchElementException() : DSEditException() { };
    NoSuchElementException(string s) : DSEditException(s) { };
    NoSuchElementException(QString s) : DSEditException(s) { };
};
/*!
 * This exception is thrown when an instance of a class
 * cannot be created
 */
class InstantiationException : public DSEditException { 
public:
    InstantiationException() : DSEditException() { };
    InstantiationException(string s) : DSEditException(s) { };
    InstantiationException(QString s) : DSEditException(s) { };
};
/*!
 * This exception is thrown when attempting to convert a
 * string to numbers that is not a valid number
 */
class NumberFormatException : public DSEditException { 
public:
    NumberFormatException() : DSEditException() { };
    NumberFormatException(string s) : DSEditException(s) { };
    NumberFormatException(QString s) : DSEditException(s) { };
};

/*!
 * This exception is thrown when a file cannot be loaded
 * for reading or writing
 */
class IOException : public DSEditException { 
public:
    IOException() : DSEditException() { };
    IOException(string s) : DSEditException(s) { };
    IOException(QString s) : DSEditException(s) { };
};
#endif // #ifndef EXCEPTIONS_H
