#ifndef DSSHAPELIST_H
#define DSSHAPELIST_H

class DSShape;
class QPoint;
class DSCanvas;

#include <Exceptions.h>
#include <QList>

using std::list;

/*!
 * This class surrounds a vector of DSShape objects.  It is analogous to the
 * DSShapeList object from the original Java assignment 
 */
class DSShapeList {
    QList<DSShape*>* m_list;
public:
    /*!
     * Create an empty shape list
     */
    DSShapeList();
    /*!
     * Frees the memory occupied by this object
     */
    ~DSShapeList();
    /*!
     * Returns the number of elements in the list
     */
     int size ();
    /*!
     * Add a new shape to the end of the list
     */
    void add(DSShape* s);  
    /*!
     * Add a new shape to the front of the list
     */
    void addFirst(DSShape* s);
    /*!
     * Add shapes to the end of the list
     */
    void addList(DSShapeList& sl);
    /*!
     * Add shapes to the start of the list
     */
    void addFirstList(DSShapeList& sl);
    /*!
     * returns the index of this shape in the list, or -1 if not found
     */
    int getIndex (DSShape* s);
    /*!
     * Inserts a shape in this index of the list
     */
    void insert (int i, DSShape* s);
    /*!
     * swaps the elements in position i and j
     */
    void swap (int i, int j);
    /*!
     * Remove a single shape from the list
     */
    void remove(DSShape* s);
    /*! 
     * Remove shapes from the list
     */
    void removeList(DSShapeList& sl);
    /*! 
     * Returns true if the specified shape is in the list, false otherwise
     */
    bool contains(DSShape* s);
    /*! 
     * Find a shape containing a point.  If no such shape is found returns null
     */
    DSShape* findShape(QPoint p);   
    /*! 
     * Find a shape containing a point and has a vertex at that point.  
     * If no such shape is found returns null
     */
    DSShape* findShapePoint(QPoint p);
    /*! 
     * Paint all the shapes in the list by calling paint on each shape
     */
    void paint();
    /*! 
     * Paint all the shapes in the list by calling paintSelect on each shape
     */
    void paintSelect();
    /*! 
     * Read DSShapes stored in a file into this list.  Throws an exception if 
     * the file is not readable or contains invalid data
     */
    void read(const QString& sourcename) throw (MalformedShapeException, IOException);
    /*! 
     * Writes the list to a specified file
     */
    void write(const QString& filename);
    /*!
     * Sets the parent canvas for all the shapes in the list
     */
    void setCanvas(DSCanvas& canvas);
};

#endif // #ifndef DSSHAPELIST_H
