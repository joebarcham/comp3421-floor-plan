#include <DSCanvas.h>
#include <DSShapeList.h>
#include <DSGLWidget.h>
#include <DSMainWindow.h>

#include <Tool.h>
#include <SelectTool.h>
#include <TranslateTool.h>
#include <RotateTool.h>
#include <ReshapeTool.h>
#include <ScaleTool.h>

#include <PolygonTool.h>
#include <WallTool.h>
#include <WindowTool.h>
#include <DoorTool.h>
#include <LightTool.h>
#include <PortalTool.h>
#include <ViewPointTool.h>

#include <QtGui>

using namespace std;

typedef QHash<QString, Tool*>::iterator HI;

DSCanvas::DSCanvas(DSMainWindow& mainWindow) : QWidget(&mainWindow) {
    m_currentTool = 0;
    m_modified = false;
    m_untitled = true;

    m_mainWindow = &mainWindow;

    m_shapes = new DSShapeList();
    m_selection = 0;

    // create tools
    m_tools = new QHash<QString, Tool*>();
    Tool* selectTool = new SelectTool(*m_mainWindow, this);
    Tool* translateTool = new TranslateTool(*m_mainWindow, this);
    Tool* polygonTool = new PolygonTool(*m_mainWindow, this);
    Tool* rotateTool = new RotateTool(*m_mainWindow, this);
    Tool* reshapeTool = new ReshapeTool(*m_mainWindow, this);
    Tool* scaleTool = new ScaleTool(*m_mainWindow, this);
    Tool* wallTool = new WallTool(*m_mainWindow, this);
    Tool* windowTool = new WindowTool(*m_mainWindow, this);
    Tool* doorTool = new DoorTool(*m_mainWindow, this);
    Tool* lightTool = new LightTool(*m_mainWindow, this);
    Tool* portalTool = new PortalTool(*m_mainWindow, this);
    Tool* viewpointTool = new ViewPointTool(*m_mainWindow, this);

    m_tools->insert(selectTool->getName(), selectTool);
    m_tools->insert(translateTool->getName(), translateTool);
    m_tools->insert(polygonTool->getName(), polygonTool);
    m_tools->insert(rotateTool->getName(), rotateTool);
    m_tools->insert(reshapeTool->getName(), reshapeTool);
    m_tools->insert(scaleTool->getName(), scaleTool);
    m_tools->insert(wallTool->getName(), wallTool);
    m_tools->insert(windowTool->getName(), windowTool);
    m_tools->insert(doorTool->getName(), doorTool);
    m_tools->insert(lightTool->getName(), lightTool);
    m_tools->insert(portalTool->getName(), portalTool);
    m_tools->insert(viewpointTool->getName(), viewpointTool);

    // set up the layout
    m_scrollArea = new QScrollArea(this);
    m_drawable = new DSGLWidget(this);
    m_scrollArea->setWidget(m_drawable);
    m_scrollArea->setFocusProxy(m_drawable);

    // connect signals from drawable to parent's signals
    connect(m_drawable, SIGNAL(mouseMoved(QMouseEvent*)), this,
            SLOT(handleMouseMoveEvent(QMouseEvent*)));
    connect(m_drawable, SIGNAL(mousePressed(QMouseEvent*)), this,
            SLOT(handleMousePressEvent(QMouseEvent*)));
    connect(m_drawable, SIGNAL(mouseReleased(QMouseEvent*)), this,
            SLOT(handleMouseReleaseEvent(QMouseEvent*)));
    connect(m_drawable, SIGNAL(keyPressed(QKeyEvent*)), this,
            SLOT(handleKeyPressEvent(QKeyEvent*)));
    connect(m_drawable, SIGNAL(keyReleased(QKeyEvent*)), this,
            SLOT(handleKeyReleaseEvent(QKeyEvent*)));

    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_scrollArea);
    setLayout(m_layout);

    setAttribute(Qt::WA_DeleteOnClose);
}

DSCanvas::~DSCanvas() {
    for (HI i = m_tools->begin(); i != m_tools->end(); i++) {
        Tool* tool = i.value();
        delete tool;
    }
    delete m_tools;
    delete m_shapes;
    delete m_drawable;
    delete m_scrollArea;
    delete m_layout;
}

void DSCanvas::newFile() {
    static int fileNum = 1;
    m_filename = QString("untitled %1").arg(fileNum);
    setWindowTitle(m_filename);
    fileNum++;
}

void DSCanvas::openFile(const QString& filename) {
    try {
        m_shapes->read(filename);
        m_shapes->setCanvas(*this);
        this->repaint();
        m_filename = filename;
        m_untitled = false;
        setWindowTitle(m_filename);
    } catch (IOException e) {
        QMessageBox::critical(this, "Unable to open the file",
                QString("File not found - %1").arg(filename),
                QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
        newFile();
    } catch (MalformedShapeException e) {
        QMessageBox::critical(this, "Unable to open the file",
                "The input file contained a malformed shape\n" +
                QString::fromStdString(e.getMessage()),
                QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
        newFile();
    }
}

bool DSCanvas::save() {
    if (m_untitled) {
        return saveAs();
    } else {
        return saveFile(m_filename);
    }
}

bool DSCanvas::saveAs() {
    if (m_untitled) // if untitled, save to current dir by default
        m_filename = QString(".");

    // prompt user for a filename
    QString fileName = QFileDialog::getSaveFileName(
            this, "Save As", m_filename, "Floor Plan (*.fpe)");
    if (fileName.isEmpty()) {
        return false;
    }
    if (saveFile(fileName)) {
        m_filename = fileName;
        m_untitled = false;
    return true;
    }
    return false;
}

bool DSCanvas::saveFile(const QString& filename) {
    m_shapes->write(filename);
    m_modified = false;
    m_untitled = false;
    setWindowTitle(filename);
    return true;
}

bool DSCanvas::closeFile() {
    if (m_modified) {
        // open up a message box
        QString msg = QString("'%1' has been modified.\n"
                "Do you want to save your changes?").arg(m_filename);
        int ret = QMessageBox::warning(this, ("MDI"),
                msg, QMessageBox::Yes | QMessageBox::Default,
                QMessageBox::No, QMessageBox::Cancel | QMessageBox::Escape);
        if (ret == QMessageBox::Yes) {
            if (!save()) {
                return false;
            }
        } else if (ret == QMessageBox::Cancel) {
            return false;
        }
    }
    return true;
}

void DSCanvas::repaint() {
    m_drawable->repaint();
}

DSShapeList& DSCanvas::getShapes() {
    return *m_shapes;
}

void DSCanvas::select(DSShape* r) {
    m_selection = r;
}

DSShape* DSCanvas::getSelection() {
    return m_selection;
}

int DSCanvas::getXOffset() {
    return m_scrollArea->horizontalScrollBar()->value();
}

int DSCanvas::getYOffset() {
    return m_scrollArea->verticalScrollBar()->value();
}

void DSCanvas::modified() {
    setWindowTitle(m_filename + QString(" (*)"));
    m_modified = true;
}

void DSCanvas::closeEvent(QCloseEvent* event) {
    if (closeFile()) {
        if (m_currentTool)
            m_currentTool->cancel();
        event->accept();
    } else {
        event->ignore();
    }
}

GLuint DSCanvas::getTexture(QString& texture) {
    return m_drawable->getTexture(texture);
}

Tool* DSCanvas::getCurrentTool() {
    return m_currentTool;
}

void DSCanvas::toolChanged(const QString& name) {
    if (m_currentTool)
        m_currentTool->cancel();
    m_currentTool = m_tools->find(name).value();
    m_drawable->setCursor(m_currentTool->getCursor());
}

void DSCanvas::handleMouseMoveEvent(QMouseEvent* evt) {
    m_currentTool->handleMouseMoveEvent(evt);
}

void DSCanvas::handleMousePressEvent(QMouseEvent* evt) {
    const char* methName = "DSCanvas::handleMousePressEvent(): ";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    m_currentTool->handleMousePressEvent(evt);
}

void DSCanvas::handleMouseReleaseEvent(QMouseEvent* evt) {
    m_currentTool->handleMouseReleaseEvent(evt);
}

void DSCanvas::handleKeyPressEvent(QKeyEvent* evt) {
    m_currentTool->handleKeyPressEvent(evt);
}

void DSCanvas::handleKeyReleaseEvent(QKeyEvent* evt) {
    m_currentTool->handleKeyReleaseEvent(evt);
}
