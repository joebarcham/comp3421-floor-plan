#ifndef POLYGONTOOL_H
#define POLYGONTOOL_H

#include <Tool.h>

class DSPolygon;
class DSMainWindow;
class QMouseEvent;

/*!
 * Polygon tool for graphical editor: implements the tool for interactive
 * creation of polygon shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class PolygonTool : public Tool {
public:
    /*!
     * PolygonTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    PolygonTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~PolygonTool();
    /*!
     * Deletes the polygon currently being drawn
     */
    void cancel();
protected:
    /*!
     * The polygon currently being created.
     */
    DSPolygon* m_p;

    /*!
     * Handles mouse click events - if a polygon is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new polygon.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse move events - if a polygon is being drawn, moves the 
     * current point under the cursor. Does nothing otherwise.
     */
    void mouseMoved  (QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);
};


#endif // #ifndef POLYGONTOOL_H
