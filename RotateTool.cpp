//------------------------------------------------------------------------------
// RotateTool
//------------------------------------------------------------------------------
// Select tool for image map editor
//------------------------------------------------------------------------------

#include <RotateTool.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>
#include <Point2D.h>

#include <QCursor>
#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

#include <sstream>
#include <iostream>

#include <math.h>
//------------------------------------------------------------------------------
// RotateTool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, init the window and canvas using parent's constructor.
//------------------------------------------------------------------------------

RotateTool::RotateTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    :Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_shapeSelected = 0;          // init shape pointer to NULL
    m_centre = 0;
    prevAngle = 0;

    // remove default cursor
    delete m_cursor;

    m_name    = new QString("Rotate");
    m_message = new QString("Click and drag an object to rotate it");
    m_cursor  = new QCursor(Qt::OpenHandCursor);
}

//------------------------------------------------------------------------------
// RotateTool Destructor
//------------------------------------------------------------------------------
RotateTool::~RotateTool(){
}

//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// On button down, highlight the region, and display a message.
//------------------------------------------------------------------------------
void RotateTool::mouseClicked(QMouseEvent* evt){
    const char* methName = "RotateTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    m_shapeSelected = m_canvas->getShapes().findShape(getPoint(evt));

    if (m_shapeSelected && prevAngle == 0) {
        if (m_centre == NULL)
            m_centre = new Point2D(m_shapeSelected->centre());
        double xDist = (m_centre->x() - evt->pos().x());
        double yDist = (m_centre->y() - evt->pos().y());
        prevAngle = atan2(yDist, xDist);
    }

    m_canvas->select(m_shapeSelected);

    // refresh canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If polygon exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------
void RotateTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "RotateTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_shapeSelected) {    
        double xDist = (m_centre->x() - evt->pos().x());
        double yDist = (m_centre->y() - evt->pos().y());
    
        double newAngle = atan2(yDist, xDist);
        double angle = newAngle - prevAngle;

        m_shapeSelected->rotate(*m_centre, angle);
        m_canvas->repaint();

        if (debug) {
            std::stringstream ss;
            ss << prevAngle << "->" << newAngle << ": rotaion amount: " << angle << std::endl;
            std::cout << ss.str();
        }
        
        prevAngle = newAngle;
    }

}

void RotateTool::mouseReleased(QMouseEvent* evt){
    if (m_centre != NULL)
        delete m_centre;
    m_centre = NULL;
    prevAngle = 0;
}