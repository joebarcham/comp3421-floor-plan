#ifndef DSVIEWPOINT_H
#define DSVIEWPOINT_H

#include <DSShape.h>
#include <QVector>
#include <Point2D.h>

class DSCanvas;
class QPolygon;
class QPoint;
class QString;
//class Point2D;
class Matrix2D;
class StringTokenizer;

/*!
 * This class represents a viewpoint shape.
 */
class DSViewPoint : public DSShape {
public:
    /*!
     * Create a new, empty viewpoint
     */
    DSViewPoint();
    /*!
     * Copy constructor
     */
    DSViewPoint(DSViewPoint& p);
    /*!
     * Free all memory being occupied by the viewpoint
     */
    ~DSViewPoint();
    /*!
     * Causes the viewpoint to draw itself on the currently active GL window
     */
    void paint();
    /*!
     * Causes the viewpoint to be drawn with the vertices highlighted, imitating a
     * selected state
     */
    void paintSelect();
    /*!
     * Returns true if the input point is contained by this viewpoint
     */
    bool contains(int x, int y);
    /*!
     * Move the viewpoint in the specified direction
     */
    void translate(int deltaX, int deltaY);
    /*!
     * Rotate the viewpoint through the specified angle around the specified pivot
     * point
     */
    void rotate(const Point2D& fixed, double angle);

    void scale(double xFactor, double yFactor);

    /*!
     * Return the centre of the viewpoint
     */
    Point2D centre();
    /*!
     * Add a point to the viewpoint
     */
    void addPoint(Point2D p);
    void addPoint(QPoint p);
    /*!
     * Select a control point.  Returns the nearest control point or (-1, -1) 
     * if none selected
     */
    QPoint selectPoint(const QPoint& p);
    /*! 
     * Remove the currently selected control point
     */
    void removePoint();
    /*!
     * Replace the currently selected control point with the input point
     */
    void setPoint(QPoint p);
    /*!
     * Apply a matrix to each point in this viewpoint
     */
    void apply(const Matrix2D& t);
    /*!
     * Return a string representation of this viewpoint
     */
    QString toString();
    /*!
     * Initialise this viewpoint from a string
     */
    void fromTokens(StringTokenizer& st) throw(MalformedShapeException);;
    /*!
     * Return a copy of this viewpoint
     */
    DSShape* clone();

protected:
    /*!
     * The points of the viewpoint in the order that they were added
     */
    QVector<Point2D>* m_endPoints;
    /*!
     * The index of the currently selected point
     */
    int m_selection;

private:
    static const double line_width = 1.0;
    static const double head_size = 10.0;
    static const double selection_dist = 10.0;
    // distance for pickling
    static const int EPSILON = 12;
    static const int TEXTURE_WIDTH  = 128;
    static const int TEXTURE_HEIGHT = 128;

    static double sqr(double x); // square of a double
};
#endif // #ifndef DSVIEWPOINT_H
