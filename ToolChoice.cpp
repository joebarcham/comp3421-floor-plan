#include <ToolChoice.h>
#include <Tool.h>
#include <SelectTool.h>
#include <TranslateTool.h>
#include <RotateTool.h>
#include <ReshapeTool.h>
#include <ScaleTool.h>

#include <PolygonTool.h>
#include <WallTool.h>
#include <WindowTool.h>
#include <DoorTool.h>
#include <LightTool.h>
#include <PortalTool.h>
#include <ViewPointTool.h>

#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>

#include <QDoubleValidator>
#include <QDir>
#include <QStringList>
#include <QtGui>

ToolChoice::ToolChoice(DSMainWindow& mainWindow, QWidget* parent) : 
    QWidget(parent) {
    m_mainWindow = &mainWindow;
    
    // create tools
    Tool* polygonTool = new PolygonTool(*m_mainWindow, 0);
    Tool* selectTool = new SelectTool(*m_mainWindow, 0);
    Tool* translateTool = new TranslateTool(*m_mainWindow, 0);
    Tool* rotateTool = new RotateTool(*m_mainWindow, 0);
    Tool* reshapeTool = new ReshapeTool(*m_mainWindow, 0);
    Tool* scaleTool = new ScaleTool(*m_mainWindow, 0);
    Tool* wallTool = new WallTool(*m_mainWindow, 0);
    Tool* windowTool = new WindowTool(*m_mainWindow, 0);
    Tool* doorTool = new DoorTool(*m_mainWindow, 0);
    Tool* lightTool = new LightTool(*m_mainWindow, 0);
    Tool* portalTool = new PortalTool(*m_mainWindow, 0);
    Tool* viewpointTool = new ViewPointTool(*m_mainWindow, 0);

    m_tools = new QHash<QString, Tool*>();
    m_tools->insert(selectTool->getName(), selectTool);
    m_tools->insert(translateTool->getName(), translateTool);
    m_tools->insert(polygonTool->getName(), polygonTool);
    m_tools->insert(rotateTool->getName(), rotateTool);
    m_tools->insert(reshapeTool->getName(), reshapeTool);
    m_tools->insert(scaleTool->getName(), scaleTool);
    m_tools->insert(wallTool->getName(), wallTool);
    m_tools->insert(windowTool->getName(), windowTool);
    m_tools->insert(doorTool->getName(), doorTool);
    m_tools->insert(lightTool->getName(), lightTool);
    m_tools->insert(portalTool->getName(), portalTool);
    m_tools->insert(viewpointTool->getName(), viewpointTool);

    m_toolCombo = new QComboBox();
    QList<QString> keys = m_tools->keys();
    for (QList<QString>::iterator i = keys.begin(); 
        i != keys.end(); i++) {
        m_toolCombo->addItem(*i);
    }

    connect(m_toolCombo, SIGNAL(activated(const QString&)),
            this, SLOT(toolChanged(const QString&)));
            
    m_extraLabel = new QLabel();
    m_extraValue = new QLineEdit(this);
    m_extraValidator = new QDoubleValidator(m_extraValue);
    m_extraValue->setValidator(m_extraValidator);
    m_extraValue->setFixedSize(QSize(60, m_extraValue->height()));
    m_extraValue->setText(QString("1.0"));
    m_extraValue->setMaxLength(7);

    connect(m_toolCombo, SIGNAL(activated(const QString&)),
                this, SLOT(toolChanged(const QString&)));
    m_gridLabel = new QLabel("Grid size: ");
    m_gridValue = new QLineEdit(this);
    m_gridValidator = new QIntValidator(m_gridValue);
    m_gridValue->setValidator(m_gridValidator);
    m_gridValue->setText(QString("8"));
    m_gridValue->setFixedSize(QSize(30, m_gridValue->height()));
    m_gridValue->setMaxLength(2);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(m_toolCombo);
    layout->addWidget(m_extraLabel);
    layout->addWidget(m_extraValue);
    layout->addWidget(m_gridLabel);
    layout->addWidget(m_gridValue);
    setLayout(layout);

    m_toolCombo->setCurrentIndex(0);
    toolChanged(m_toolCombo->itemText(0));
}

ToolChoice::~ToolChoice() {
    delete m_toolCombo;
    delete m_gridValidator;
    delete m_extraValidator;

    delete m_gridLabel;
    delete m_gridValue;
    delete m_extraLabel;
    delete m_extraValue;
    
    for (QHash<QString, Tool*>::iterator i = m_tools->begin(); 
            i != m_tools->end(); i++) {
        Tool* tool = i.value();
        delete tool;
    } 
    delete m_tools;
}

int ToolChoice::getGridSize() const {
    return m_gridValue->text().toInt();
}

QString ToolChoice::getTool() const {
    return m_toolCombo->currentText();
}

void ToolChoice::setExtra1(double extra) {
    m_extraValue->setText(QString::number(extra));

}

void ToolChoice::setExtra2(double extra) {
    m_extraValue->setText(QString::number(extra));

}

double ToolChoice::getExtra1() const {
    return m_extraValue->text().toDouble();
}

double ToolChoice::getExtra2() const {
    return m_extraValue->text().toDouble();
}

void ToolChoice::toolChanged(const QString& toolName) {
    Tool* tool = m_tools->find(toolName).value();
    m_mainWindow->toolChanged(*tool);
    QString extra = tool->getExtra();
    bool showExtra = extra != "";
    m_extraLabel->setText(extra);
    m_extraLabel->setVisible(showExtra);
    m_extraValue->setVisible(showExtra);
}
