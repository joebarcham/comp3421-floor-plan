#ifndef DSLIGHT_H
#define DSLIGHT_H

#include <DSShape.h>
#include <QVector>
#include <Point2D.h>

class DSCanvas;
class QPolygon;
class QPoint;
class QString;
//class Point2D;
class Matrix2D;
class StringTokenizer;

/*!
 * This class represents a light shape.
 */
class DSLight : public DSShape {
public:
    /*!
     * Create a new, empty light
     */
    DSLight();
    /*!
     * Copy constructor
     */
    DSLight(DSLight& p);
    /*!
     * Free all memory being occupied by the light
     */
    ~DSLight();
    /*!
     * Causes the light to draw itself on the currently active GL window
     */
    void paint();
    /*!
     * Causes the light to be drawn with the vertices highlighted, imitating a
     * selected state
     */
    void paintSelect();
    /*!
     * Returns true if the input point is contained by this light
     */
    bool contains(int x, int y);
    /*!
     * Move the light in the specified direction
     */
    void translate(int deltaX, int deltaY);
    /*!
     * Rotate the light through the specified angle around the specified pivot
     * point
     */
    void rotate(const Point2D& fixed, double angle);

    void scale(double xFactor, double yFactor);

    /*!
     * Return the centre of the light
     */
    Point2D centre();
    /*!
     * Add a point to the light
     */
    void addPoint(Point2D p);
    void addPoint(QPoint p);
    /*!
     * Select a control point.  Returns the nearest control point or (-1, -1) 
     * if none selected
     */
    QPoint selectPoint(const QPoint& p);
    /*! 
     * Remove the currently selected control point
     */
    void removePoint();
    /*!
     * Replace the currently selected control point with the input point
     */
    void setPoint(QPoint p);
    void setPoint(Point2D p);
    /*!
     * Apply a matrix to each point in this light
     */
    void apply(const Matrix2D& t);
    /*!
     * Return a string representation of this light
     */
    QString toString();
    /*!
     * Initialise this light from a string
     */
    void fromTokens(StringTokenizer& st) throw(MalformedShapeException);;
    /*!
     * Return a copy of this light
     */
    DSShape* clone();

protected:
    /*!
     * The points of the light in the order that they were added
     */
    Point2D* m_Position;
    bool initialised;
    double radius;

private:
    static const int line_count = 100;

    static double sqr(double x); // square of a double
};
#endif // #ifndef DSLIGHT_H
