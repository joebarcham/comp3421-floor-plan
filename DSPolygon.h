#ifndef DSPOLYGON_H
#define DSPOLYGON_H

#include <DSShape.h>
#include <QVector>
#include <Point2D.h>

class DSCanvas;
class QPolygon;
class QPoint;
class QString;
//class Point2D;
class Matrix2D;
class StringTokenizer;

/*!
 * This class represents a polygon shape.  It is analogous to the DSPolygon
 * class in the original Java assignment
 */
class DSPolygon : public DSShape {
public:
    /*!
     * Create a new, empty polygon
     */
    DSPolygon();
    /*!
     * Copy constructor
     */
    DSPolygon(DSPolygon& p);
    /*!
     * Free all memory being occupied by the polygon
     */
    ~DSPolygon();
    /*!
     * Causes the polygon to draw itself on the currently active GL window
     */
    void paint();
    /*!
     * Causes the polygon to be drawn with the vertices highlighted, imitating a
     * selected state
     */
    void paintSelect();
    /*!
     * Returns true if the input point is contained by this polygon
     */
    bool contains(int x, int y);
    /*!
     * Move the polygon in the specified direction
     */
    void translate(int deltaX, int deltaY);
    /*!
     * Rotate the polygon through the specified angle around the specified pivot
     * point
     */
    void rotate(const Point2D& fixed, double angle);
    
    void scale(double xFactor, double yFactor);

    /*!
     * Return the centre of the polygon
     */
    Point2D centre();
    /*!
     * Add a point to the polygon
     */
    void addPoint(QPoint p);
    /*!
     * Select a control point.  Returns the nearest control point or (-1, -1) 
     * if none selected
     */
    QPoint selectPoint(const QPoint& p);
    /*! 
     * Remove the currently selected control point
     */
    void removePoint();
    /*!
     * Replace the currently selected control point with the input point
     */
    void setPoint(QPoint p);
    /*!
     * Apply a matrix to each point in this polygon
     */
    void apply(const Matrix2D& t);
    /*!
     * Return a string representation of this polygon
     */
    QString toString();
    /*!
     * Initialise this polygon from a string
     */
    void fromTokens(StringTokenizer& st) throw(MalformedShapeException);;
    /*!
     * Return a copy of this polygon
     */
    DSShape* clone();
protected:
    /*!
     * The points of the polygon in the order that they were added
     */
    QVector<Point2D>* m_pts2d;
    /*!
     * The index of the currently selected point
     */
    int m_selection;
    /* 
     * find the closest edge of a polygon to p and select end point
     * and return poition on that edge as number between 0 and 1
     */
    double closestEdge(QPolygon& polygon, QPoint& p);
private:
    // distance for pickling
    static const int EPSILON = 12;
    static const int TEXTURE_WIDTH  = 128;
    static const int TEXTURE_HEIGHT = 128;
    // Convert list of Point2D to a QPolygon
    static QPolygon toPolygon(const QVector<Point2D>& ps);
    static double sqr(double x); // square of a double
};
#endif // #ifndef DSPOLYGON_H
