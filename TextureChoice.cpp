#include <TextureChoice.h>
#include <DSMainWindow.h>

#include <QDir>
#include <QStringList>
#include <QtGui>

#include <sstream>

TextureChoice::TextureChoice(QWidget* parent) : QWidget(parent) {
    m_textureLabel = new QLabel("Texture: ");
    m_textureList = new QComboBox();
    // get the list of files in the textures dir
    QDir textureDir(QString("textures"));
    QStringList textureFiles = textureDir.entryList(QDir::Files);
    //add No Texture Choice first
    QVariant data(QString(""));
    m_textureList->addItem("None", data);
    for (QStringList::const_iterator i = textureFiles.begin();
            i != textureFiles.end(); i++) {
        if (/* (*i).endsWith(".jpg", Qt::CaseInsensitive) ||
            (*i).endsWith(".jpeg", Qt::CaseInsensitive)|| */
            (*i).endsWith(".png", Qt::CaseInsensitive)) {

            QVariant data(*i);
            m_textureList->addItem(*i, data);
        }
    }
    
    m_scaleLabel = new QLabel("Scale: ");
    m_scaleValue = new QLineEdit(this);
    m_scaleValidator = new QDoubleValidator(m_scaleValue);
    m_scaleValue->setValidator(m_scaleValidator);
    m_scaleValue->setText(QString("1.0"));
    m_scaleValue->setFixedSize(QSize(60, m_scaleValue->height()));
    m_scaleValue->setMaxLength(7);
    
    m_colourLabel = new QLabel("Colour: ");
    m_colourButton = new QPushButton();
    m_currentColour = QColor(255, 255, 255);
    setBackgroundColour(*m_colourButton, m_currentColour);
    connect(m_colourButton, SIGNAL(clicked()), this, 
            SLOT(showColourDialog()));

    

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(m_colourLabel);
    layout->addWidget(m_colourButton);
    layout->addWidget(m_textureLabel);
    layout->addWidget(m_textureList);
    layout->addWidget(m_scaleLabel);
    layout->addWidget(m_scaleValue);
    setLayout(layout);
}

TextureChoice::~TextureChoice() {
    delete m_scaleValidator;

    delete m_colourLabel;
    delete m_colourButton;
    delete m_textureLabel;
    delete m_textureList;
    delete m_scaleLabel;
    delete m_scaleValue;
}


QColor& TextureChoice::getColour() {
    return m_currentColour;
}

void TextureChoice::setColour(QColor colour) {
    m_currentColour = colour;
    setBackgroundColour(*m_colourButton, m_currentColour);
}

QString TextureChoice::getTexture() const {
    return m_textureList->itemData(m_textureList->currentIndex()).toString();
}

void TextureChoice::setTexture(QString& texture) {
    m_textureList->setCurrentIndex(m_textureList->findData(QVariant(texture)));
}

void TextureChoice::setScale(double scale) {
    m_scaleValue->setText(QString::number(scale));
}

double TextureChoice::getScale() const {
    return m_scaleValue->text().toDouble();
}

void TextureChoice::showColourDialog() {
    m_currentColour = m_colourDialog->getColor();
    setBackgroundColour(*m_colourButton, m_currentColour);
}

void TextureChoice::setBackgroundColour(QPushButton& button, 
        QColor& colour) {
    std::stringstream ss;
    ss << "QPushButton {background-color: rgb(" 
        << colour.red() << "," << colour.green() 
        << "," << colour.blue() << ");}";
    button.setStyleSheet(QString(ss.str().c_str()));
}
