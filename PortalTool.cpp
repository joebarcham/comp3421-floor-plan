//------------------------------------------------------------------------------
// PortalTool
//------------------------------------------------------------------------------
// Portal tool for graphical editor: implements the tool for interactive
// creation of portal shapes.
//------------------------------------------------------------------------------

#include <PortalTool.h>
#include <DSPortal.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// PortalTool Constructor
//------------------------------------------------------------------------------
// Init portal to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

PortalTool::PortalTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init portal pointer to NULL
    lines_drawn = 0;
    
    delete m_extra;
    m_name    = new QString("Portal");
    m_message = new QString("Click and drag twice to create portal");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// PortalTool Destructor
//------------------------------------------------------------------------------
// If a portal was created, destroy it.
//------------------------------------------------------------------------------

PortalTool::~PortalTool(){
    if (m_p)
        delete m_p;
}

void PortalTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no portal being drawn, create a new one and add this point to
//   it. The running portal is added to the canvas.
// - If shift is held down while clicking, the local portal is destroyed
//   (The finished portal continues to reside in the canvas).
// - Otherwise just add another point to the portal.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void PortalTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "PortalTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no portal in progress
        m_p = new DSPortal();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add portal to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this portal from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    } else if (lines_drawn == 1) {
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    m_p->addPoint(gridPoint(evt));  // add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If portal exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void PortalTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "PortalTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // portal in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void PortalTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "PortalTool::mouseReleased): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    ++lines_drawn;

    if (lines_drawn == 2) {
        m_p = 0;
        lines_drawn = 0;
    }
}
