//------------------------------------------------------------------------------
// DoorTool
//------------------------------------------------------------------------------
// Door tool for graphical editor: implements the tool for interactive
// creation of door shapes.
//------------------------------------------------------------------------------

#include <DoorTool.h>
#include <DSDoor.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// DoorTool Constructor
//------------------------------------------------------------------------------
// Init door to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

DoorTool::DoorTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init door pointer to NULL
    
    delete m_extra;
    m_name    = new QString("Door");
    m_message = new QString("Click and drag to create door");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// DoorTool Destructor
//------------------------------------------------------------------------------
// If a door was created, destroy it.
//------------------------------------------------------------------------------

DoorTool::~DoorTool(){
    if (m_p)
        delete m_p;
}

void DoorTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no door being drawn, create a new one and add this point to
//   it. The running door is added to the canvas.
// - If shift is held down while clicking, the local door is destroyed
//   (The finished door continues to reside in the canvas).
// - Otherwise just add another point to the door.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void DoorTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "DoorTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no door in progress
        m_p = new DSDoor();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add door to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this door from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    m_p->addPoint(gridPoint(evt));  // add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If door exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void DoorTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "DoorTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // door in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void DoorTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "DoorTool::mouseReleased): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    m_p = 0;
}
