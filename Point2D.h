#ifndef POINT2D_H
#define POINT2D_H


#include <QString>
#include <QPoint>
           
/*! 
 * This class represents a 2D point.  Like a QPoint, but
 * uses floats instead of ints.
 */
class Point2D {
private:
    double m_x;
    double m_y;
public:
    /*!
     * Create a new point representing the origin (0, 0)
     */
    Point2D();
    /*!
     * Create a new point given its' Cartesian coordinates
     */
    Point2D(double x, double y);
    /*!
     * Create a new point given a QPoint
     */
    Point2D(const QPoint& p);
    /*!
     * Free all memory being occupied by this object
     */
    ~Point2D();
    /*!
     * Get the x co-ordinate
     */
    double x() const;
    /*!
     * Get the y co-ordinate
     */
    double y() const;
    // interpolate this + t*(q-this);
    /*!
     * Interpolate this point
     */
    Point2D interp(Point2D& q, double t);
    /*!
     * Distance from this to p
     */
    double dist(const Point2D& p) const;
    /*!
     * Distance from u to closest point on line segment a->b
     */
    static double distToSeg(const Point2D& a, const Point2D& b, const Point2D& u);
    /*!
     * Returns a string representation of this point
     */
    QString toString() const;
    /*!
     * Return the QPoint object corresponding to this point.  Note that the
     * co-ordinates will be rounded to integer values
     */
    QPoint toPoint() const;
    /*!
     * Round a double down to an integer value
     */
    static int round(double num);
};

#endif // #ifndeif POINT2D_H 
