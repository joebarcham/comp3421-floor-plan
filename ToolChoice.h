#ifndef TOOLCHOICE_H
#define TOOLCHOICE_H

#include <QWidget>
#include <QHash>

class QComboBox;
class QLabel;
class QLineEdit;
class QIntValidator;
class QDoubleValidator;

class Tool;
class DSMainWindow;

/*!
 * ToolChoice is a widget that allows the current tool and grid size to
 * be chosen.
 */
class ToolChoice : public QWidget {
    Q_OBJECT
    QComboBox*        m_toolCombo;
    DSMainWindow*     m_mainWindow;
    QLabel*           m_gridLabel;          // grid
    QLineEdit*        m_gridValue;
    QIntValidator*    m_gridValidator;
    QLabel*           m_extraLabel;         // extra
    QLineEdit*        m_extraValue;
    QDoubleValidator* m_extraValidator;
    QHash<QString, Tool*>* m_tools;
public:
    /*!
     * Construct a new instance of this object given a DSMainWindow.  It will 
     * connect the signal of the tool combo box selection change to the main
     * window's corresponding signal
     */
    ToolChoice(DSMainWindow& mainWindow, QWidget* parent = 0);
    /*!
     * Frees all memory being occupied by this object
     */
    ~ToolChoice();
    /*!
     * Get the grid size
     */
    int getGridSize() const;
    /*!
     * Get the name of the currently selected tool
     */
    QString getTool() const;
    /*!
     * Get the currently selected height/road width
     */
    double getExtra1() const;
    double getExtra2() const;
    /*!
     * Set the currently selected height/road width
     */
    void setExtra1(double extra);
    void setExtra2(double extra);

private slots:
    void toolChanged(const QString& toolName);
};

#endif // #ifndef TOOLCHOICE_H
