#ifndef DSDOOR_H
#define DSDOOR_H

#include <DSShape.h>
#include <QVector>
#include <Point2D.h>

class DSCanvas;
class QPolygon;
class QPoint;
class QString;
//class Point2D;
class Matrix2D;
class StringTokenizer;

/*!
 * This class represents a door shape.
 */
class DSDoor : public DSShape {
public:
    /*!
     * Create a new, empty door
     */
    DSDoor();
    /*!
     * Copy constructor
     */
    DSDoor(DSDoor& p);
    /*!
     * Free all memory being occupied by the door
     */
    ~DSDoor();
    /*!
     * Causes the door to draw itself on the currently active GL window
     */
    void paint();
    /*!
     * Causes the door to be drawn with the vertices highlighted, imitating a
     * selected state
     */
    void paintSelect();
    /*!
     * Returns true if the input point is contained by this door
     */
    bool contains(int x, int y);
    /*!
     * Move the door in the specified direction
     */
    void translate(int deltaX, int deltaY);
    /*!
     * Rotate the door through the specified angle around the specified pivot
     * point
     */
    void rotate(const Point2D& fixed, double angle);

    void scale(double xFactor, double yFactor);

    /*!
     * Return the centre of the door
     */
    Point2D centre();
    /*!
     * Add a point to the door
     */
    void addPoint(Point2D p);
    void addPoint(QPoint p);
    /*!
     * Select a control point.  Returns the nearest control point or (-1, -1) 
     * if none selected
     */
    QPoint selectPoint(const QPoint& p);
    /*! 
     * Remove the currently selected control point
     */
    void removePoint();
    /*!
     * Replace the currently selected control point with the input point
     */
    void setPoint(QPoint p);
    /*!
     * Apply a matrix to each point in this door
     */
    void apply(const Matrix2D& t);
    /*!
     * Return a string representation of this door
     */
    QString toString();
    /*!
     * Initialise this door from a string
     */
    void fromTokens(StringTokenizer& st) throw(MalformedShapeException);;
    /*!
     * Return a copy of this door
     */
    DSShape* clone();

protected:
    /*!
     * The points of the door in the order that they were added
     */
    QVector<Point2D>* m_endPoints;
    /*!
     * The index of the currently selected point
     */
    int m_selection;

private:
    static const double line_width = 2.0;
    static const double selection_dist = 10.0;

    static const int arc_line_count = 100;

    // distance for pickling
    static const int EPSILON = 12;
    static const int TEXTURE_WIDTH  = 128;
    static const int TEXTURE_HEIGHT = 128;

    static double sqr(double x); // square of a double
};
#endif // #ifndef DSDOOR_H
