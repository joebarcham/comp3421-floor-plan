#ifndef EDGE2D_H
#define EDGE2D_H


#include <Point2D.h>
#include <Matrix2D.h>

class Matrix2D;
class QPoint;
class Point2D;
class QString;

/*!
 * This class represents a 2D line segment made of two Point2Ds
 */
class Edge2D {
    Point2D m_start;
    Point2D m_end;
    Matrix2D m_toLineSpaceT;
public:
    /*!
     * Create an new instance of this object given the endpoints
     */
    Edge2D(const Point2D& start, const Point2D& end);
    /*!
     * Create a new instance of this object given the co-ordinates for the
     * endpoints
     */
    Edge2D(int x1, int y1, int x2, int y2);
    /*!
     * Frees all memory occupied by this object
     */
    ~Edge2D();
    /*!
     * Returns a string representation of this object
     */
    QString toString() const;    
    /*!         
     * Transforms this to line space (t,s) where s is distance from line and 
     * t tells you how far you are along line.  Returns the result of the
     * transformation
     */
    Point2D toLineSpace(const QPoint& p);
private:
    void init();
};

#endif // #ifndef EDGE2D_H
