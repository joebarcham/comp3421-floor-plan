//------------------------------------------------------------------------------
// ScaleTool
//------------------------------------------------------------------------------
// Select tool for image map editor
//------------------------------------------------------------------------------

#include <ScaleTool.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>
#include <Point2D.h>

#include <QCursor>
#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

#include <sstream>
#include <iostream>

#include <math.h>
//------------------------------------------------------------------------------
// ScaleTool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, init the window and canvas using parent's constructor.
//------------------------------------------------------------------------------

ScaleTool::ScaleTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    :Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_shapeSelected = 0;          // init shape pointer to NULL
    m_centre = 0;
    xPrevDist = 0;
    yPrevDist = 0;

    // remove default cursor
    delete m_cursor;

    m_name    = new QString("Scale");
    m_message = new QString("Click and drag an object to scale it");
    m_cursor  = new QCursor(Qt::OpenHandCursor);
}

//------------------------------------------------------------------------------
// ScaleTool Destructor
//------------------------------------------------------------------------------
ScaleTool::~ScaleTool(){
}

//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// On button down, highlight the region, and display a message.
//------------------------------------------------------------------------------
void ScaleTool::mouseClicked(QMouseEvent* evt){
    const char* methName = "ScaleTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    m_shapeSelected = m_canvas->getShapes().findShape(getPoint(evt));

    if (m_shapeSelected && (xPrevDist == 0 || yPrevDist == 0)) {
        if (m_centre == NULL)
            m_centre = new Point2D(m_shapeSelected->centre());
        xPrevDist = m_centre->x() - evt->pos().x();
        yPrevDist = m_centre->y() - evt->pos().y();
    }

    m_canvas->select(m_shapeSelected);

    // refresh canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If polygon exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------
void ScaleTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "ScaleTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_shapeSelected) {    
        double xDist = m_centre->x() - evt->pos().x();
        double yDist = m_centre->y() - evt->pos().y();

        if (xDist != 0 && yDist != 0) {
            double xScale = xDist / xPrevDist;
            double yScale = yDist / yPrevDist;

            m_shapeSelected->translate(-1*m_centre->x(), -1*m_centre->y());
            m_shapeSelected->scale(xScale, yScale);
            m_shapeSelected->translate(m_centre->x(), m_centre->y());

            m_canvas->repaint();
            
            xPrevDist = xDist;
            yPrevDist = yDist;
        }
    }

}

void ScaleTool::mouseReleased(QMouseEvent* evt){
    if (m_centre != NULL)
        delete m_centre;
    m_centre = NULL;
    xPrevDist = 0;
    yPrevDist = 0;
}