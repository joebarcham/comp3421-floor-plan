//------------------------------------------------------------------------------
// StringTokenizer
//------------------------------------------------------------------------------
// Breaks a string into tokens based on delimiter regexp, then returns tokens
// one by one through nextToken().
//
// Delimiter used is a regular expression (QRegExp style):
// - to OR, use square brackets (e.g. [ab] matches a or b)
// - to OR a range, use [-] (e.g. [A-Z] matches A or B or C or ... or Y or Z)
// - quantifier {,} tells how many instances of expression are needed
//   (e.g. x{1,2} means at least 1 and at most 2 instances of x)
// - use () and | for multi-char OR nottation (e.g. (bike|car|train) matches any 
//   of those words).
// - \d matches digit, \D non-digit
// - \s matches whitespace, \S non-whitespace
//------------------------------------------------------------------------------

#include <QStringList>
#include <QString>
#include <QRegExp>

#include <StringTokenizer.h>


//------------------------------------------------------------------------------
// StringTokenizer Constructor
//------------------------------------------------------------------------------
// Create the list of "words" out of str, separated by regexp delimiter delim.
// To create a delimiter that matches either space or tab use delim string
// "[ \t]"
//------------------------------------------------------------------------------

StringTokenizer::StringTokenizer(QString& str, QString& delim) {
    m_stringList = str.split(QRegExp(delim));
}

StringTokenizer::StringTokenizer(QString& str, const char* delim){
    m_stringList = str.split(QRegExp(delim));
}


//------------------------------------------------------------------------------
// StringTokenizer Destructor
//------------------------------------------------------------------------------
// List is on the stack, nothing to delete
//------------------------------------------------------------------------------

StringTokenizer::~StringTokenizer(){
}


//------------------------------------------------------------------------------
// nextToken
//------------------------------------------------------------------------------
// Get the first string in the list and return it. 
// The string is deleted from the list. 
// 
// WARNING: If there are no more strings in the list, the return value is 
//          unknown. Use hasMoreTokens() if you are unsure whether there are 
//          more tokens left.
//------------------------------------------------------------------------------

QString StringTokenizer::nextToken() throw (NoSuchElementException) {
    if (m_stringList.isEmpty()) {
        throw NoSuchElementException(); 
    }
    QString token = m_stringList.first();  // get 1st string in list
    m_stringList.pop_front();              // delete 1st string from list
    return token;
}


//------------------------------------------------------------------------------
// hasMoreTokens
//------------------------------------------------------------------------------
// Return true if there are still tokens in the queue, false otherwise.
//------------------------------------------------------------------------------

bool StringTokenizer::hasMoreTokens(){
    return !(m_stringList.isEmpty());
}
