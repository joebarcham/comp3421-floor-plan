#include <DSShapeList.h>
#include <DSShape.h>
#include <QPoint>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <fstream>
#include <iostream>

using namespace std;

// iterator for the lists
typedef QList<DSShape*>::const_iterator LI;

DSShapeList::DSShapeList() {
    m_list = new QList<DSShape*>();
}

DSShapeList::~DSShapeList() {
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        delete *i;
    }
    delete m_list;
}

int DSShapeList::size() {
    return m_list->count();
}

void DSShapeList::add(DSShape* s) {
    const char* methName = "DSShapeList::add():";
    bool debug = false;
    if (debug) {
        printf("%s called. Shape: %x size: %d\n", methName, (unsigned int) &s, m_list->size());
    }
    m_list->push_back(s);
    if (debug) {
        printf("%s exiting. Size: %d\n", methName, m_list->size());
    }
}

void DSShapeList::addFirst(DSShape* s) {
    const char* methName = "DSShapeList::addFirst():";
    bool debug = false;
    if (debug) {
        printf("%s called. Shape: %x size: %d\n", methName, (unsigned int) &s, m_list->size());
    }
    m_list->push_front(s);
    if (debug) {
        printf("%s exiting. Size: %d\n", methName, m_list->size());
    }
}

//TODO: Test the addList functions
void DSShapeList::addList(DSShapeList& sl) {
    for (LI i = sl.m_list->constBegin(); i != sl.m_list->constEnd(); i++) {
        m_list->push_back(*i);
    }
}

void DSShapeList::addFirstList(DSShapeList& sl) {
    for (LI i = sl.m_list->constBegin(); i != sl.m_list->constEnd(); i++) {
        m_list->push_front(*i);
    }
}

int DSShapeList::getIndex (DSShape* s) {
    return m_list->indexOf(s);
}

void DSShapeList::insert (int i, DSShape* s) {
    m_list->insert(i,s);
}           

void DSShapeList::swap (int i, int j) {
    m_list->swap(i,j);
}

void DSShapeList::remove(DSShape* s) {
    m_list->removeAll(s);
}

void DSShapeList::removeList(DSShapeList& sl) {
    for (LI i = sl.m_list->constBegin(); i != sl.m_list->constEnd(); i++) {
        m_list->removeAll(*i);
    }
}

bool DSShapeList::contains(DSShape* s) {
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        if (*i == s) {
            return true;
        }
    }
    return false;
}

DSShape* DSShapeList::findShape(QPoint p) {
    //search backwards so frontmost shape is found
    for (LI i = m_list->constEnd() - 1; i >= m_list->constBegin(); --i) {
        DSShape* shape = *i;
        if (shape->contains(p.x(), p.y())) {
            return shape;
        }
    }
    return 0;
}

DSShape* DSShapeList::findShapePoint(QPoint p) {
    //search backwards so frontmost shape is found
    for (LI i = m_list->constEnd() - 1; i >= m_list->constBegin(); --i) {
        DSShape* shape = *i;
        if (shape->contains(p.x(), p.y()) && shape->selectPoint(p) != QPoint(-1,-1)) {
            return shape;
        }
    }
    return 0;
}

void DSShapeList::paint() {
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        DSShape* shape = *i;
        shape->paint();
    }
}

void DSShapeList::paintSelect() {
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        DSShape* shape = *i;
        shape->paintSelect();
    }
}

void DSShapeList::read(const QString& sourcename) 
    throw (MalformedShapeException, IOException) {
    ifstream ins;
    QString s = "";
    string next;

    ins.open(sourcename.toStdString().c_str());

    if (ins.fail( )) {
        throw(IOException(sourcename));
    }

    getline(ins, next);
    while (!ins.eof()){
        s = next.c_str();
        try {
            add(DSShape::create(s));
        } catch (MalformedShapeException e) {
            printf("Shape: %s\n", next.c_str());
        }
        getline(ins, next);
    }
    ins.close();
}

void DSShapeList::write(const QString& filename){
    QFile file(filename); // open up output file

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        ;  // TODO: message box saying can't open file to write
    }

    QTextStream out(&file); // make a stream to write to file

    // iterate throught the object list and write object strings to file
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        DSShape* shape = *i;
        out << (shape->toString()) << "\n";
    }
}

void DSShapeList::setCanvas(DSCanvas& canvas) {
    for (LI i = m_list->constBegin(); i != m_list->constEnd(); i++) {
        DSShape* shape = *i;
        shape->setCanvas(canvas);
    }
}
