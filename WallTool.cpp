//------------------------------------------------------------------------------
// WallTool
//------------------------------------------------------------------------------
// Wall tool for graphical editor: implements the tool for interactive
// creation of wall shapes.
//------------------------------------------------------------------------------

#include <WallTool.h>
#include <DSWall.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// WallTool Constructor
//------------------------------------------------------------------------------
// Init wall to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

WallTool::WallTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init wall pointer to NULL
    
    delete m_extra;
    m_name    = new QString("Wall");
    m_message = new QString("Click and drag to create wall");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// WallTool Destructor
//------------------------------------------------------------------------------
// If a wall was created, destroy it.
//------------------------------------------------------------------------------

WallTool::~WallTool(){
    if (m_p)
        delete m_p;
}

void WallTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no wall being drawn, create a new one and add this point to
//   it. The running wall is added to the canvas.
// - If shift is held down while clicking, the local wall is destroyed
//   (The finished wall continues to reside in the canvas).
// - Otherwise just add another point to the wall.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void WallTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "WallTool::mouseClicked(): ";
    bool debug = true;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no wall in progress
        m_p = new DSWall();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add wall to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this wall from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    m_p->addPoint(gridPoint(evt));  // add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If wall exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void WallTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "WallTool::mouseDragged(): ";
    bool debug = true;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // wall in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void WallTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "WallTool::mouseReleased): ";
    bool debug = true;

    if (debug)
        printf("%s called\n", methName);

    m_p = 0;
}
