#include <DSWall.h>
#include <Edge2D.h>
#include <Matrix2D.h>
#include <Point2D.h>
#include <StringTokenizer.h>
#include <Exceptions.h>

#include <QPolygon>
#include <QPoint>
#include <QVector>
#include <QRegion>
#include <QtOpenGL>

#include "DSCanvas.h"

#include <iostream>
#include <math.h>

using namespace std;


DSWall::DSWall() : DSShape() {
    m_endPoints = new QVector<Point2D>();
    m_selection = 0;
    m_name = "Wall";
}

DSWall::DSWall(DSWall& p) : DSShape(p) {
    m_selection = p.m_selection;
    m_endPoints = new QVector<Point2D>();
    addPoint(p.m_endPoints->at(0));
    addPoint(p.m_endPoints->at(1));
    m_fill = p.m_fill;
}

DSWall::~DSWall() {
    delete m_endPoints;
}

void DSWall::paint() {
    setGLColour();
    glDisable(GL_TEXTURE_2D);
    glLineWidth(line_width);

    if (m_endPoints->size() > 0) {
        glBegin(GL_LINES);  //draw the wall 
        for (int i = 0; i < m_endPoints->size(); i++) {
            Point2D p = m_endPoints->at(i);
            glVertex2d(p.x(), p.y());
        }
        glEnd(); 
    }
}

void DSWall::paintSelect() {
    for(int i = 0; i < m_endPoints->size(); i++){
        Point2D p = m_endPoints->at(i);
        drawPoint(p.x(), p.y());
    }
}

bool DSWall::contains(int x, int y) {
    return selection_dist >= Point2D::distToSeg(m_endPoints->at(0), m_endPoints->at(1), Point2D(x, y));
}

void DSWall::rotate(const Point2D& fixed, double angle) {
    const Matrix2D matrix = Matrix2D::rotateAbout(fixed, angle);
    apply(matrix);
}

void DSWall::translate(int deltax, int deltay) {
    const Matrix2D matrix = Matrix2D(1, 0, deltax, 0, 1, deltay);
    apply(matrix);
}

void DSWall::scale(double xFactor, double yFactor) {
    const Matrix2D matrix = Matrix2D(xFactor, 0, 0, 0, yFactor, 0);
    apply(matrix);
}

void DSWall::addPoint(Point2D p) {
    addPoint(p.toPoint());
}

void DSWall::addPoint(QPoint p) {
    if (m_endPoints->size() < 2) {
        m_endPoints->append(p);
        m_selection = m_endPoints->size() - 1;
    }
}


void DSWall::setPoint(QPoint p) {
    if (m_selection >= 0) {
        m_endPoints->replace(m_selection, p);
    }
}

QPoint DSWall::selectPoint(const QPoint& p) {
    double mind = 0;
    m_selection = -1;
    for (int i = 0; i < m_endPoints->size(); i++) {
        Point2D p2 = m_endPoints->at(i);
        double d = sqr(p2.x() - p.x()) + sqr(p2.y() - p.y());
        if ((d < mind || m_selection == -1) && d < EPSILON*EPSILON) {
            mind = d;
            m_selection = i;
        }
    }
    if (m_selection == -1) {
        return QPoint(-1, -1);
    }
    QPoint result = m_endPoints->at(m_selection).toPoint();
    return result;
}

void DSWall::removePoint() {
    if (m_selection >= 0) {
        m_endPoints->remove(m_selection);
    }
}

Point2D DSWall::centre() {
    Point2D a = m_endPoints->at(0);
    Point2D b = m_endPoints->at(1);

    return Point2D((a.x()+b.x())/2, (a.y()+b.y())/2);
}

void DSWall::apply(const Matrix2D& t){
    for (int i = 0; i < m_endPoints->size(); i++){
        const Point2D p = t.apply(m_endPoints->at(i));
        m_endPoints->replace(i, p);
    }
}

QString DSWall::toString() {
    QString result = DSShape::toString();

    result += " " + QString::number(m_endPoints->size());
    for (int i = 0; i < m_endPoints->size(); i++) {
        Point2D p = m_endPoints->at(i);
        result += " ";
        result += p.toString();
    }

    return result;
}

void DSWall::fromTokens(StringTokenizer& st) throw(MalformedShapeException) {
    try {
        bool ok;
        m_selection = -1;
        int n = (st.nextToken()).toInt(&ok);  // how many points
        if ( !ok ) {
                throw NumberFormatException();
        }
        for (int i = 0; i < n; i++){
            double x = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            double y = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            addPoint(Point2D(x, y));
        }
    }
    catch (NoSuchElementException e) {
        throw (MalformedShapeException("NoSuchElementException " +
                    e.getMessage()));
    }
    catch (NumberFormatException e) {
        throw (MalformedShapeException("NumberFormatException " + 
                    e.getMessage()));
    }
}

double DSWall::sqr(double x) {
    return x*x;
}

DSShape* DSWall::clone() {
    return new DSWall(*this);
}
