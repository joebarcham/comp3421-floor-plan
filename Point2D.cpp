#include <Point2D.h>

#include <math.h>

Point2D::Point2D() {
    ;
}

Point2D::Point2D(double x, double y) {
    m_x = x;
    m_y = y;
}

Point2D::Point2D(const QPoint& p) {
    m_x = p.x();
    m_y = p.y();
}

Point2D::~Point2D() {
   ; // nothing to be done. 
}

double Point2D::x() const {
    return m_x;
}

double Point2D::y() const {
    return m_y;
}

Point2D Point2D::interp(Point2D& q, double t) {
    return Point2D(m_x + t*(q.m_x - m_x), m_y + t*(q.m_y - m_y));
}

double Point2D::dist(const Point2D& p) const {
    return sqrt( (m_x - p.x())*(m_x - p.x()) + (m_y - p.y())*(m_y - p.y()) );
}

QString Point2D::toString() const {
    return QString("%1 %2").arg(m_x).arg(m_y);
}

QPoint Point2D::toPoint() const {
    return QPoint(round(m_x), round(m_y));
}

double Point2D::distToSeg(const Point2D& a, const Point2D& b, const Point2D& u) {
    double length = (a.x()-b.x())*(a.x()-b.x()) + (a.y()-b.y())*(a.y()-b.y());
    if (length == 0) return a.dist(u);
    double uax = u.x() - a.x();
    double uay = u.y() - a.y();
    double bax = b.x() - a.x();
    double bay = b.y() - a.y();
    double t = (uax*bax + uay*bay) / length;
    if (t < 0) return a.dist(u);
    else if (t > 1) return b.dist(u);
    Point2D projection = Point2D(a.x() + t*(b.x()-a.x()), a.y() + t*(b.y()-a.y()));
    return projection.dist(u);
}

int Point2D::round(double num){
    int trunc = (int) num, offset = 1;
    if ( num < 0 ) {
        offset = -1;
    }
    if ( (num * offset) - (trunc * offset) < 0.5) {
        return (int) floor(num);
    } else {
        return (int) ceil(num);
    }
}

