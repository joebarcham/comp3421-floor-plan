#ifndef MATRIX2D_H
#define MATRIX2D_H

class QPoint;
class QString;
class Point2D;

/*! 
 * This class implements 2D affine transformation matrices. The last row is
 * always assumed to be (0, 0, 1).
 */
class Matrix2D {
    // first row
    double m_xx;
    double m_xy;
    double m_xw;
    // second row
    double m_yx;
    double m_yy;
    double m_yw;
    // last row is  0, 0, 1
    
    /*!
     * Rounds the double to the nearest int (e.g. rounds a 3.4 down to 3, but 
     * 3.5 up to 4)
     */
    int round(double num);

public:
    /*! 
     * Default constructor. This creates a zero matrix - if applied to a point
     * results in origin (0, 0, 1), if composed with a matrix results in a zero
     * matrix
     */
    Matrix2D();
    /*!
     * Matrix constructor - give it the top 2 rows of the matrix (the last row
     * is always (0, 0, 1)).
     */
    Matrix2D(double xx, double xy, double xw, double yx, double yy, double yw);
    ~Matrix2D();

    /*!
     * Composition of two transformations - the result of first doing this
     * one and then the one supplied as a parameter. 
     * Use this to compound a series of transformations into one. E.g. if you 
     * need to translate 100 points and then rotate them, instead of applying 
     * the translation and then rotation matrix 100 times (total 200 matrix
     * applications), compose the 2 matrices and apply that to the points (100 
     * matrix applications in total).
     */
    Matrix2D compose(const Matrix2D& m) const;
    /*!
     * Apply this transformation to a QPoint (QPoint is a pair of ints).
     */
    QPoint apply(const QPoint& p) const;
    /*!
     * Apply this transformation to a Point2D (Point2D is a pair of doubles).
     */
    Point2D apply(const Point2D& p) const;
    /*!
     * Returns a rotation about point c by given angle matrix.
     */
    static Matrix2D rotateAbout(const Point2D& c, double angle);
    /*!
     * Returns a scaling about point c by given ratio matrix.
     */
    static Matrix2D scaleAbout(const Point2D& c, double sx, double sy);

    /*!
     * Returns string representation of matrix. Useful for debugging.
     */
    QString toString() const;
};

#endif // #ifndef MATRIX2D_H
