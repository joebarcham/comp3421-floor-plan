#ifndef DOORTOOL_H
#define DOORTOOL_H

#include <Tool.h>

class DSDoor;
class DSMainWindow;
class QMouseEvent;

/*!
 * Door tool for graphical editor: implements the tool for interactive
 * creation of door shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class DoorTool : public Tool {
public:
    /*!
     * DoorTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    DoorTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~DoorTool();
    /*!
     * Deletes the door currently being drawn
     */
    void cancel();
protected:
    /*!
     * The door currently being created.
     */
    DSDoor* m_p;

    /*!
     * Handles mouse click events - if a door is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new door.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef DOORTOOL_H
