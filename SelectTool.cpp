//------------------------------------------------------------------------------
// SelectTool
//------------------------------------------------------------------------------
// Select tool for image map editor
//------------------------------------------------------------------------------

#include <SelectTool.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>

#include <QCursor>
#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// SelectTool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, init the window and canvas using parent's constructor.
//------------------------------------------------------------------------------

SelectTool::SelectTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    :Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_shapeSelected = 0;          // init shape pointer to NULL

    // remove default cursor
    delete m_cursor;

    m_name    = new QString("Select");
    m_message = new QString("Click an object to select it");
    m_cursor  = new QCursor(Qt::PointingHandCursor);
}

//------------------------------------------------------------------------------
// SelectTool Destructor
//------------------------------------------------------------------------------
SelectTool::~SelectTool(){
}

//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// On button down, highlight the region, and display a message.
//------------------------------------------------------------------------------
void SelectTool::mouseClicked(QMouseEvent* evt){
    const char* methName = "SelectTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    m_shapeSelected = m_canvas->getShapes().findShape(getPoint(evt));
    m_canvas->select(m_shapeSelected);

    // refresh canvas
    m_canvas->repaint();
}
