#ifndef PROPERTIES_DIALOG_H
#define PROPERTIES_DIALOG_H

#include <QColor>
#include <QDialog>

class TextureChoice;
class DSMainWindow;
class DSShape;

class QLabel;
class QLineEdit;
class QLabel;
class QComboBox;
class QColorDialog;
class QString;
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;
class QDoubleValidator;

/*!
 * The properties dialog is used to change the colour, texture and
 * other properties of an existant DSShape
 */
class PropertiesDialog : public QDialog {
    Q_OBJECT
    QColor            m_currentColour;
    TextureChoice*    m_textureChoice;
    QPushButton*      m_okButton;
    QPushButton*      m_cancelButton;
    QHBoxLayout*      m_buttonsLayout;
    QHBoxLayout*      m_propsLayout;
    QVBoxLayout*      m_mainLayout;
    DSShape*          m_currentShape;
    QLabel*           m_extraLabel;
    QLineEdit*        m_extraValue;
    QDoubleValidator* m_extraValidator;

public:
    /*!
     * Create a new instance of this class
     */
    PropertiesDialog(QWidget* parent = 0);
    /*!
     * Free all memory occupied by this object
     */
    ~PropertiesDialog();
    /*! 
     * Get the properties from the shape and show them.  This method
     * must be called before QDialog::exec() 
     */
    void setShape(DSShape& s);
private slots:
    /* 
     * Set the currently shown properties to the current shape 
     */
    void updateProperties();
};
#endif // #ifndef PROPERTY_DIALOG_H
