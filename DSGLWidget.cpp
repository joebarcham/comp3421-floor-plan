#include <DSGLWidget.h>
#include <DSCanvas.h>
#include <DSShapeList.h>
#include <DSShape.h>

#include <QSize>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QtOpenGL>

DSGLWidget::DSGLWidget(DSCanvas* parent) {
    m_parent = parent;
    setMouseTracking(true);  
    m_textures = new QHash<QString, GLuint>();   
    m_textureTally = new QHash<QString, GLuint>();
    initialiseGL();
}

DSGLWidget::~DSGLWidget() {
    makeCurrent();
    //Each Texture is removed from the gl texture memory 
    //before the hashtable is destroyed
    QHash<QString, GLuint>::const_iterator i;
    for (i = m_textures->constBegin(); i != m_textures->constEnd(); ++i)
        deleteTexture(i.value());
    m_textures->clear();
    delete m_textures;   
    delete m_textureTally;
}

QSize DSGLWidget::sizeHint() const {
    return QSize(DSGLWidget::WIDTH, DSGLWidget::HEIGHT);
}

void DSGLWidget::initialiseGL() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //black
}

void DSGLWidget::paintGL() {
    //used to detect unused textures
    //note: texture deletion is currently deactivated
    //resetTextureCount();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    int xoffset = m_parent->getXOffset();
    int yoffset = m_parent->getYOffset();
    glOrtho(xoffset, xoffset + width(), yoffset + height(), yoffset, -1, 1);
    /** Clear the colour buffer */
    glClear(GL_COLOR_BUFFER_BIT);
    DSShapeList& shapes = m_parent->getShapes();
    shapes.paint();
    DSShape* selection = m_parent->getSelection();
    if (selection != 0) {
        selection->paintSelect();

        //deletes any textues from gl memory that 
    //have not been used this paint call
    //note: remove this line to disable texture deletion
    //and the call to resetTextureCount[optional]
    //note: texture deletion is currently deactivated
        //removeUnsedTextures();
    }
}

void DSGLWidget::resizeGL(int width, int height) {
    glViewport(0, 0, width, height);
}

void DSGLWidget::mousePressEvent(QMouseEvent* event) {
    const char* methName = "DSGLWidget::mousePressEvent";
    bool debug = false;
    if (debug) {        
        printf("%s called\n", methName);
    }
    emit mousePressed(event);
}

void DSGLWidget::mouseMoveEvent(QMouseEvent* event) {    
    const char* methName = "DSGLWidget::mouseMoveEvent";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    emit mouseMoved(event);
}

void DSGLWidget::mouseReleaseEvent(QMouseEvent* event) {
    emit mouseReleased(event);
}

void DSGLWidget::keyPressEvent(QKeyEvent* event) {
    const char* methName = "DSGLWidget::keyPressEvent";
    bool debug = false;
    if (debug) {
        printf("%s called\n", methName);
    }
    emit keyPressed(event);
}

void DSGLWidget::keyReleaseEvent(QKeyEvent* event) {
    emit keyReleased(event);
}

GLuint DSGLWidget::getTexture(QString& texture) {
    GLuint handle;
    if (!m_textures->contains(texture)){
        handle = bindTexture(QImage(QString("textures/" + texture)));
        m_textures->insert(texture, handle);
    } 
    else  {                   
        handle = m_textures->value(texture);
    }   

    // marks this texture as being used
    if (!m_textureTally->contains(texture))
        m_textureTally->insert(texture, handle);
    return handle;
}

void DSGLWidget::resetTextureCount() {
    m_textureTally->clear();
}

void DSGLWidget::removeUnsedTextures() {
    //iterates through the m_textures hash, and 
    //deletes any texture that has not been used
    //since the last call to resetTextureCount()
    QHash<QString, GLuint>::iterator i;
    for (i = m_textures->begin(); i != m_textures->end();) {
        if (!m_textureTally->contains(i.key())) {
            deleteTexture(i.value());
            i = m_textures->erase(i);
        } else
           ++i;
    }
    m_textureTally->clear();
}
