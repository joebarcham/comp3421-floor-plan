#ifndef TOOL_H
#define TOOL_H

class DSCanvas;
class DSMainWindow;
class QCursor;
class QPoint;
class QMouseEvent;
class QKeyEvent;
class QString;

/*!
 * This class represents an abstract tool. The common functionality of all tools
 * (such as select, translate, scale) is defined here. Inherit from this class 
 * when creating concrete tool subclasses (scale & rotate).
 */
class Tool {
public:
    /*!
     * Deletes common tool members. By having the destructor virtual, if you 
     * delete a class derived from this one, both the destructors will get 
     * called implicitly. That is, there is no need to delete the common Tool 
     * members (cursor, message, name) in the subclass's destructor, as those 
     * will be deleted by this destructor.
     */
    virtual ~Tool();
    /*!
     * Called to cancel the tool's current operation (if it's performing any).
     * This function is called if the current tool is changed prematurely from
     * the main window.  Subclasses will need to override this function if there
     * is anything involved in resetting them.
     */
    virtual void cancel();
    /*!
     * Returns the name of the tool to be displayed in a menu.
     */
    QString& getName() const;
    /*!
     * Returns the message to be displayed in the status bar when this tool is 
     * selected
     */
    QString& getMessage() const;
    /*!
     * Returns the cursor to use when this tool is active.
     */
    QCursor& getCursor() const;
    /*!
     * Returns the name of the extra field to show in ToolChoice.  
     * Default is the empty string.  Override to change.
     */
    QString& getExtra() const;
    /*!
     * Returns the exact mouse position.
     */
    QPoint getPoint(QMouseEvent* evt);
    /*!
     * Returns the mouse position rounded to the nearest grid point.
     */
    QPoint gridPoint(QMouseEvent* evt);
    /*!
     * Captures mouse movement events and sorts them into either mouse drag
     * events (moving mouse with the button down) or simple mouse movement 
     * events. These are then passed to mouseMoved() or mouseDragged().
     */
    void handleMouseMoveEvent(QMouseEvent* evt);
    /*!
     * Captures mouse press events
     */
    void handleMousePressEvent(QMouseEvent* evt);
    /*!
     * Captures mouse release events and hands off to mouseReleased()
     */
    void handleMouseReleaseEvent(QMouseEvent* evt);
    /*!
     * This function receives key press events, just in case you need them.
     */
    void handleKeyPressEvent(QKeyEvent* evt);
    /*!
     * This function receives key release events, just in case you need them.
     */
    void handleKeyReleaseEvent(QKeyEvent* evt);
    
protected:
    /*!
     * The canvas that this tool operates on
     */
    DSCanvas* m_canvas;
    /*!
     * The window that holds the canvas
     */
    DSMainWindow* m_mainWindow;
    /*!
     * The cursor associated with this tool. If you want to reset it in the 
     * derived class, first delete the default cursor assigned by Tool's 
     * constructor.
     */
    QCursor* m_cursor;
    /*!
     * The name associated with this tool - set this in derived class's
     * constructor
     */
    QString* m_name;
    /*!
     * The message to be displayed in the status bar when this tool is selected.
     * Set this in derived class's constructor
     */
    QString* m_message;
    /*!
     * The name of the extra field for this tool
     */
    QString* m_extra;
    /*!
     * The tool constructor. Creates a new instance given a main window and a
     * canvas. Call this constructor from subclassess constructors'
     * initialisation lists to initialise common members. Note that because this
     * is an abstract class, you can only call this constructor from this 
     * class's children
     */
    Tool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mousePressed(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseClicked(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseReleased(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseEntered(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseExited(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseDragged(QMouseEvent* evt);
    /*!
     * Implement this method in Tool's subclass to make it do something.
     */
    virtual void mouseMoved(QMouseEvent* evt);
};

#endif //ifndef TOOL_H
