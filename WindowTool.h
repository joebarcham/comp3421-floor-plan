#ifndef WINDOWTOOL_H
#define WINDOWTOOL_H

#include <Tool.h>

class DSWindow;
class DSMainWindow;
class QMouseEvent;

/*!
 * Window tool for graphical editor: implements the tool for interactive
 * creation of window shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class WindowTool : public Tool {
public:
    /*!
     * WindowTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    WindowTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~WindowTool();
    /*!
     * Deletes the window currently being drawn
     */
    void cancel();
protected:
    /*!
     * The window currently being created.
     */
    DSWindow* m_p;

    /*!
     * Handles mouse click events - if a window is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new window.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef WINDOWTOOL_H
