//------------------------------------------------------------------------------
// Matrix2D
//------------------------------------------------------------------------------
// This class implents 2D affine transformation matrices.
//------------------------------------------------------------------------------

#include <Matrix2D.h>
#include <Point2D.h>
#include <QPoint>
#include <QString>
#include <math.h>     // for rounding and trig

//------------------------------------------------------------------------------
// Matrix2D Constructor
//------------------------------------------------------------------------------
// Set coords to the given ones.
//------------------------------------------------------------------------------

Matrix2D::Matrix2D(double xx, double xy, double xw, double yx, double yy, double yw){
    m_xx = xx;
    m_xy = xy;
    m_xw = xw;
    m_yx = yx;
    m_yy = yy;
    m_yw = yw;
}


//------------------------------------------------------------------------------
// Matrix2D Default Constructor
//------------------------------------------------------------------------------
// Set coords to 0.
//------------------------------------------------------------------------------
Matrix2D::Matrix2D(){
    m_xx = 0;
    m_xy = 0;
    m_xw = 0;
    m_yx = 0;
    m_yy = 0;
    m_yw = 0;
}


//------------------------------------------------------------------------------
// Matrix2D Destructor
//------------------------------------------------------------------------------
// No allocated members - nothing to destroy.
//------------------------------------------------------------------------------
Matrix2D::~Matrix2D(){
}


//------------------------------------------------------------------------------
// compose
//------------------------------------------------------------------------------
// Composition of two transformations - the resulting of first doing this
// one and then the one supplied as a parameter.
//------------------------------------------------------------------------------
Matrix2D Matrix2D::compose(const Matrix2D& m) const{
    double xx = m.m_xx * m_xx + m.m_xy * m_yx;
    double xy = m.m_xx * m_xy + m.m_xy * m_yy;
    double xw = m.m_xx * m_xw + m.m_xy * m_yw + m.m_xw;
    double yx = m.m_yx * m_xx + m.m_yy * m_yx;
    double yy = m.m_yx * m_xy + m.m_yy * m_yy;
    double yw = m.m_yx * m_xw + m.m_yy * m_yw + m.m_yw;
    return Matrix2D(xx, xy, xw, yx, yy, yw);
}


//------------------------------------------------------------------------------
// apply
//------------------------------------------------------------------------------
// Apply a transformation to a point.
//------------------------------------------------------------------------------
QPoint Matrix2D::apply(const QPoint& p) const {
    return QPoint(Point2D::round(m_xx*p.x() + m_xy*p.y() + m_xw),
                Point2D::round(m_yx*p.x() + m_yy*p.y() + m_yw));
}


//------------------------------------------------------------------------------
// apply
//------------------------------------------------------------------------------
// Apply a transformation to a point.
//------------------------------------------------------------------------------
Point2D Matrix2D::apply(const Point2D& p) const {
    return Point2D(m_xx*p.x() + m_xy*p.y() + m_xw,
            m_yx*p.x() + m_yy*p.y() + m_yw);
}


//------------------------------------------------------------------------------
// rotateAbout
//------------------------------------------------------------------------------
// Return the rotation about point matrix.
//------------------------------------------------------------------------------
Matrix2D Matrix2D::rotateAbout(const Point2D& c, double angle) {
    double cosA = cos(angle);
    double sinA = sin(angle);
    return Matrix2D(cosA, -sinA, -c.x()*cosA + c.y()*sinA + c.x(),
                    sinA, cosA,  -c.x()*sinA - c.y()*cosA + c.y());
}


//------------------------------------------------------------------------------
// scaleAbout
//------------------------------------------------------------------------------
// Return the scale about point matrix.
//------------------------------------------------------------------------------
Matrix2D Matrix2D::scaleAbout(const Point2D& c, double sx, double sy) {
    return Matrix2D(sx, 0, -sx*c.x() + c.x(), 0, sy, -sy*c.y() + c.y());
}


//------------------------------------------------------------------------------
// toString
//------------------------------------------------------------------------------
// Returns string representation of matrix. Useful for debugging.
//------------------------------------------------------------------------------
QString Matrix2D::toString() const {
    return QString("%1 %2 %3 | %4 %5 %6").
            arg(m_xx).arg(m_xy).arg(m_xw).arg(m_yx).arg(m_yy).arg(m_yw);
}
