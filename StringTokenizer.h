#ifndef STRINGTOKENIZER_H
#define STRINGTOKENIZER_H

#include<Exceptions.h>

class QStringList;
class QString;

/*!
 * Breaks a string into tokens based on delimiter regexp, then returns tokens
 * one by one through nextToken().
 *
 * Delimiter used is a regular expression (QRegExp style):
 * - to OR, use square brackets (e.g. [ab] matches a or b)
 * - to OR a range, use [-] (e.g. [A-Z] matches A or B or C or ... or Y or Z)
 * - quantifier {,} tells how many instances of expression are needed
 *   (e.g. x{1,2} means at least 1 and at most 2 instances of x)
 * - use () and | for multi-char OR nottation (e.g. (bike|car|train) matches any
 *   of those words).
 * - \\d matches digit, \\D non-digit
 * - \\s matches whitespace, \\S non-whitespace
 * Refer to QRegExp documentation if you need further reference.
 */
class StringTokenizer{
public:
    /*!
     * Tokenizer constructor, the first arg is the input string, the second is a
     * regular expression identifying word delimiters.
     */
    StringTokenizer(QString& str, QString& delim);
    /*!
     * Tokenizer constructor, the first arg is the input string, the second is a
     * regular expression identifying word delimiters.
     */
    StringTokenizer(QString& str, const char* delim);
    ~StringTokenizer();

    /*!
     * Returns the next token in the string. <BR>
     * <STRONG>WARNING:</STRONG> If there are no more tokens, random garbage is
     * returned. Make sure there are still tokens left to retrieve by using 
     * hasMoreTokens().
     *  
     * Throws NoSuchElementException when no more tokens exist
     */
    QString nextToken() throw (NoSuchElementException);
    /*!
     * Check if there are still more tokens in the string. Returns true if there 
     * are, false if all the tokens have already been extracted.
     */
    bool hasMoreTokens();

private:
    QStringList m_stringList; // the list of tokens in the string
};

#endif //ifndef STRINGTOKENIZER_H
