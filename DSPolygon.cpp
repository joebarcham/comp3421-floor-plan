#include <DSPolygon.h>
#include <Edge2D.h>
#include <Matrix2D.h>
#include <Point2D.h>
#include <StringTokenizer.h>
#include <Exceptions.h>

#include <QPolygon>
#include <QPoint>
#include <QVector>
#include <QRegion>
#include <QtOpenGL>

#include "DSCanvas.h"

#include <iostream>
using namespace std;


DSPolygon::DSPolygon() : DSShape() {
    m_pts2d = new QVector<Point2D>();
    m_selection = 0;
    m_name = "FPPolygon";
}

DSPolygon::DSPolygon(DSPolygon& p) : DSShape(p) {
    m_selection = p.m_selection;
    m_pts2d = new QVector<Point2D>();
    m_fill = p.m_fill;
    for (int i = 0; i < p.m_pts2d->size(); i++) {
        Point2D pt = Point2D(p.m_pts2d->at(i));
        m_pts2d->append(p.m_pts2d->at(i));
    }
}

DSPolygon::~DSPolygon() {
    delete m_pts2d;
}

void DSPolygon::paint() {
      //The "" string is reserved as 'no texture'
      if (m_texture != "" && m_texture != "null" ){
         GLuint handle = m_canvas->getTexture(m_texture);
         glBindTexture(GL_TEXTURE_2D, handle);
         
         //GL_REPLACE configures the texture to draw directly (no colour blending)
         //with the current solid colour of the polygon
         glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
         glEnable(GL_TEXTURE_2D);
      } else{  
         setGLColour();
         glDisable(GL_TEXTURE_2D);
      }

      if (m_pts2d->size() > 0) {
        /* Clamped version:
         * Point2D clamp = m_pts2d->at((m_pts2d->size() == 1) ? 0 : 1);
         */
        glBegin(GL_POLYGON);  //draw the polygon 
        for (int i = 0; i < m_pts2d->size(); i++) {
            Point2D p = m_pts2d->at(i);
	       /* Clamped version:
            * glTexCoord2d(m_scale * ((p.x() - clamp.x())/TEXTURE_WIDTH),
                        -m_scale * ((p.y() - clamp.y())/TEXTURE_HEIGHT));
	        */
            glTexCoord2d(m_scale * p.x() / TEXTURE_WIDTH,
                        -m_scale * p.y() / TEXTURE_HEIGHT);
            glVertex2d(p.x(), p.y());
         }
      }
      glEnd(); 
      glDisable(GL_TEXTURE_2D);
}

void DSPolygon::paintSelect() {
    for(int i = 0; i < m_pts2d->size(); i++){
        Point2D p = m_pts2d->at(i);
        drawPoint(p.x(), p.y());
    }
}

bool DSPolygon::contains(int x, int y) {
    const QPoint searchPt(x, y);
    QPolygon poly = toPolygon(*m_pts2d);
    QRegion region(poly);
    return region.contains(searchPt);
}

void DSPolygon::rotate(const Point2D& fixed, double angle) {
    const Matrix2D matrix = Matrix2D::rotateAbout(fixed, angle);
    apply(matrix);
}

void DSPolygon::translate(int deltax, int deltay) {
    const Matrix2D matrix = Matrix2D(1, 0, deltax, 0, 1, deltay);
    apply(matrix);
}

void DSPolygon::scale(double xFactor, double yFactor) {
    const Matrix2D matrix = Matrix2D(xFactor, 0, 0, 0, yFactor, 0);
    apply(matrix);
}

void DSPolygon::addPoint(QPoint p) {
    m_pts2d->append(p);
    m_selection = m_pts2d->size() - 1;
}


void DSPolygon::setPoint(QPoint p) {
    if (m_selection >= 0) {
        m_pts2d->replace(m_selection, p);
    }
}

QPoint DSPolygon::selectPoint(const QPoint& p) {
    double mind = 0;
    m_selection = -1;
    for (int i = 0; i < m_pts2d->size(); i++) {
        Point2D p2 = m_pts2d->at(i);
        double d = sqr(p2.x() - p.x()) + sqr(p2.y() - p.y());
        if ((d < mind || m_selection == -1) && d < EPSILON*EPSILON) {
            mind = d;
            m_selection = i;
        }
    }
    if (m_selection == -1) {
        return QPoint(-1, -1);
    }
    QPoint result = m_pts2d->at(m_selection).toPoint();
    return result;
}

void DSPolygon::removePoint() {
    if (m_selection >= 0) {
        m_pts2d->remove(m_selection);
    }
}

Point2D DSPolygon::centre() {
    Point2D p = m_pts2d->at(0);
    double minx, miny, maxx, maxy;
    minx = p.x();
    maxx = p.x(); 
    miny = p.y();
    maxy = p.y();
    for (int i = 1; i < m_pts2d->size(); i++){
        p = m_pts2d->at(i);
        if (p.x() < minx) {
            minx = p.x();
        }
        if (p.x() > maxx) {
            maxx = p.x();
        }
        if (p.y() < miny) {
            miny = p.y();
        }
        if (p.y() > maxy) {
            maxy = p.y();
        }
    }
    return Point2D((minx+maxx)/2,(miny+maxy)/2);
}

void DSPolygon::apply(const Matrix2D& t){
    for (int i = 0; i < m_pts2d->size(); i++){
        const Point2D p = t.apply(m_pts2d->at(i));
        m_pts2d->replace(i, p);
    }
}

QString DSPolygon::toString() {
    QString result = DSShape::toString();

    result += " " + QString::number(m_pts2d->size());
    for (int i = 0; i < m_pts2d->size(); i++) {
        Point2D p = m_pts2d->at(i);
        result += " ";
        result += p.toString();
    }

    return result;
}

void DSPolygon::fromTokens(StringTokenizer& st) throw(MalformedShapeException) {
    try {
        bool ok;
        m_selection = -1;
        int n = (st.nextToken()).toInt(&ok);  // how many points
        if ( !ok ) {
                throw NumberFormatException();
        }
        for (int i = 0; i < n; i++){
            double x = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            double y = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            m_pts2d->append(Point2D(x, y));
        }
    }
    catch (NoSuchElementException e) {
        throw (MalformedShapeException("NoSuchElementException " +
                    e.getMessage()));
    }
    catch (NumberFormatException e) {
        throw (MalformedShapeException("NumberFormatException " + 
                    e.getMessage()));
    }
}

QPolygon DSPolygon::toPolygon(const QVector<Point2D>& ps) {
    QPolygon result = QPolygon(ps.size());
    for (int i = 0; i < ps.size(); i++) {
        QPoint p = ps.at(i).toPoint();
        result.setPoint(i, p);
    }
    return result;
}

double DSPolygon::sqr(double x) {
    return x*x;
}

double DSPolygon::closestEdge(QPolygon& pl, QPoint& p) {
    Point2D* closest = 0;
    int plSize = pl.size();
    for (int i = 0; i < plSize; i++) {
        int iplus = (i+1) % plSize;
        QPoint start = pl.at(i);
        QPoint end = pl.at(iplus);
        Edge2D e = Edge2D(start.x(), start.y(), end.x(), end.y());
        Point2D pe = e.toLineSpace(p);
        if (pe.x() > 0 && pe.x() < 1 && abs((long) pe.y()) < EPSILON) {
            if (closest == 0 || abs((long)pe.y()) < abs((long)closest->y()) ) {
                closest = &pe;
                m_selection = iplus;
            }
        }
    }
    return (closest == 0) ? -1.0 : closest->x();
}

DSShape* DSPolygon::clone() {
    return new DSPolygon(*this);
}
