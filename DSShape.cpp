//------------------------------------------------------------------------------
// DSShape
//------------------------------------------------------------------------------
// This class represents a shape in the editor (such as a polygon).
//------------------------------------------------------------------------------

#include <DSShape.h>
#include <Point2D.h>
#include <DSCanvas.h>

#include <QPoint>
#include <QTextStream>
#include <QtOpenGL>

#include <DSPolygon.h>
#include <DSWall.h>
#include <DSWindow.h>
#include <DSLight.h>
#include <DSDoor.h>
#include <DSViewPoint.h>
#include <DSPortal.h>

#include <StringTokenizer.h>
#include <iostream>

using namespace std;

//------------------------------------------------------------------------------
// DSShape Constructor
//------------------------------------------------------------------------------
// Set fill colour to white, texture scaling to 1.
//------------------------------------------------------------------------------

DSShape::DSShape() {
    m_fill   = QColor(255, 255, 255);
    m_scale  = 1.0;
    m_name   = QString("DSShape");
    m_canvas = 0;
    m_extra1  = 1;
    m_extra2  = 1;
//    m_texture = "";
}

DSShape::DSShape(DSShape& s) {
    m_fill    = s.m_fill;
    m_scale   = s.m_scale;
    m_name    = s.m_name;
    m_texture = s.m_texture;
    m_extra1   = s.m_extra1;
    m_extra2   = s.m_extra2;
}

DSShape::~DSShape() {
    ;
}


//------------------------------------------------------------------------------
// setFillColour
//------------------------------------------------------------------------------
// Set the shape's fill colour
//------------------------------------------------------------------------------

void DSShape::setFillColour(QColor fill) {
    m_fill = fill;
}


//------------------------------------------------------------------------------
// getFillColour
//------------------------------------------------------------------------------
// Return the shape's fill colour
//------------------------------------------------------------------------------

QColor& DSShape::getFillColour() {
    return m_fill;
}


//------------------------------------------------------------------------------
// setTexture
//------------------------------------------------------------------------------
// Set the texture's filename
//------------------------------------------------------------------------------

void DSShape::setTexture(QString texture) {
    m_texture = texture;
}


//------------------------------------------------------------------------------
// getTexture
//------------------------------------------------------------------------------
// Return the texture's filename
//------------------------------------------------------------------------------

QString& DSShape::getTexture() {
    return m_texture;
}


//------------------------------------------------------------------------------
// setScale
//------------------------------------------------------------------------------
// Set the texture's scaling - larger scale will make the texture look bigger.
//------------------------------------------------------------------------------

void DSShape::setScale(double scale) {
    m_scale = scale;
}

//------------------------------------------------------------------------------
// getScale
//------------------------------------------------------------------------------
// Returns the texture's scaling.
//------------------------------------------------------------------------------

double DSShape::getScale() {
    return m_scale;
}

//------------------------------------------------------------------------------
// setExtra
//------------------------------------------------------------------------------
// Set the Height of the object in 3D (or width for roads) - only used in
// Assignment2
//------------------------------------------------------------------------------

void DSShape::setExtra1(double extra) {
    m_extra1 = extra;
}

void DSShape::setExtra2(double extra) {
    m_extra2 = extra;
}
//------------------------------------------------------------------------------
// getExtra
//------------------------------------------------------------------------------
// Gets the Height of the object in 3D (or width for roads) - only used in 
// Assignment2
//------------------------------------------------------------------------------

double DSShape::getExtra1() {
    return m_extra1;
}

double DSShape::getExtra2() {
    return m_extra2;
}

//------------------------------------------------------------------------------
// getName
//------------------------------------------------------------------------------
// Returns this class' name (used when saving to file, in toString()).
//------------------------------------------------------------------------------

QString DSShape::getName() {
    return m_name;
}

void DSShape::setCanvas(DSCanvas& canvas) {
    m_canvas = &canvas;
}

DSCanvas* DSShape::getCanvas() {
    return m_canvas;
}

//------------------------------------------------------------------------------
// drawPoint
//------------------------------------------------------------------------------
// Given the 2D coordinates, draw a point
//------------------------------------------------------------------------------

void DSShape::drawPoint(double x, double y) {
    glColor3f(1.0f, 1.0f, 1.0f);
    glPushAttrib(GL_COLOR_BUFFER_BIT); //save current Logic Op
    glEnable(GL_COLOR_LOGIC_OP);
    glLogicOp(GL_XOR);
    glBegin(GL_POLYGON);
    glVertex2d(x+SIZE,y+SIZE);
    glVertex2d(x-SIZE,y+SIZE);
    glVertex2d(x-SIZE,y-SIZE);
    glVertex2d(x+SIZE,y-SIZE);
    glEnd();
    glPopAttrib();
}


//------------------------------------------------------------------------------
// create
//------------------------------------------------------------------------------
// Creates a shape from a string describing it.  This must consist of a
// sequence of space-separated fields.  The first field is the name of a
// class that is a child of DSShape.  The next three are the colour.  The
// next is the name of the texture.  Further fields will be interpreted
// by the child's fromTokens method.  Returns the clickable region
// described by the string.  
//
// Throws a MalformedShapeException if there is problem parsing.
//------------------------------------------------------------------------------

DSShape* DSShape::create(QString& s) throw(MalformedShapeException) {
    DSShape *shape;
    QString className = "";
    try {
        StringTokenizer st = StringTokenizer(s, "\\s+");
        className = st.nextToken();          // token name

        // Unlike Java, I couldn't find an easy way to create a dynamic instance
        // of a class given it's name.  CObject is a part of Visual C++, but
        // would rather be safe than sorry. Feel free to replace
        // Therefore, I'm writing out all cases, instead of forcing users to
        // implement it on their own.  The java users wouldn't have to do that
        if (className == "FPPolygon" || className == "DSPolygon") {
            // Class name prefix has changed to FP from DS
            // so read FPPolygon but create DSPolygon
            shape = new DSPolygon();
        } else if (className == "Wall") {
            // Other shapes have no prefix
            shape = new DSWall();
        } else if (className == "Window") {
            shape = new DSWindow();
        } else if (className == "Light") {
            shape = new DSLight();
        } else if (className == "Door") {
            shape = new DSDoor();
        } else if (className == "ViewPoint") {
            shape = new DSViewPoint();
        } else if (className == "Portal") {
            shape = new DSPortal();
        } else {
            throw( ClassNotFoundException(className));
        }
        
        if ( shape == NULL ) {
            throw( InstantiationException(className));
        }

        int red   = st.nextToken().toInt();        // token red
        int green = st.nextToken().toInt();        // token green
        int blue  = st.nextToken().toInt();        // token blue

        shape->setFillColour(QColor(red, green, blue));

        QString texture = st.nextToken();     // No DSShape::setTexture(QString), only QString&
        shape->setTexture(texture);           // token texture
        shape->setScale((st.nextToken()).toDouble());// token scale
        shape->setExtra1((st.nextToken()).toDouble());// token extra
        shape->setExtra2((st.nextToken()).toDouble());// token extra

        shape->fromTokens(st);                       // parse points

        return shape;
    }
    catch(ClassNotFoundException e) {
        cerr << "Error creating shape: " << e.getMessage() << endl;
        throw(MalformedShapeException(s));
    }
    catch(InstantiationException e) {
        cerr << "Couldn't Instance " << e.getMessage() << endl;
        throw(MalformedShapeException(s));
    }
    catch(NoSuchElementException e) {
        cerr << "No such element " << e.getMessage() << endl;
        throw(MalformedShapeException(s));
    }
}

//------------------------------------------------------------------------------
// toString
//------------------------------------------------------------------------------
// Returns the string representation of this object.  It must be suitable to be
// used in the create method to recreate a copy of this object.
//------------------------------------------------------------------------------
QString DSShape::toString(){
    QString str;
    QString tex = (m_texture == ""? "null" : m_texture);

    QTextStream stream(&str);
    stream << m_name << " " << m_fill.red() << " " << m_fill.green() << " "
           << m_fill.blue() << " " << tex << " " << m_scale << " " << m_extra1
           << " " << m_extra2;

    return str;
}




//------------------------------------------------------------------------------
// setGLColour
//------------------------------------------------------------------------------
// Sets the current GL drawing colour to the current fill colour
//------------------------------------------------------------------------------

void DSShape::setGLColour() {
    glColor3ub(m_fill.red(), m_fill.green(), m_fill.blue());
}
