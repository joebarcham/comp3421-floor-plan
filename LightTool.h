#ifndef LIGHTTOOL_H
#define LIGHTTOOL_H

#include <Tool.h>

class DSLight;
class DSMainWindow;
class QMouseEvent;

/*!
 * Light tool for graphical editor: implements the tool for interactive
 * creation of light shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class LightTool : public Tool {
public:
    /*!
     * LightTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    LightTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~LightTool();
    /*!
     * Deletes the light currently being drawn
     */
    void cancel();
protected:
    /*!
     * The light currently being created.
     */
    DSLight* m_p;

    /*!
     * Handles mouse click events - if a light is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new light.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef LIGHTTOOL_H
