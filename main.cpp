#include <QApplication>
#include <QString>
#include <DSMainWindow.h>

int main(int argc, char** argv) {
    Q_INIT_RESOURCE(dsed);
    QApplication app(argc, argv);
    DSMainWindow mainWindow;
    
    if (argc > 1)
        mainWindow.openFile(argv[1]);
    else
        mainWindow.openFile(0);

    mainWindow.show();
    return app.exec();
}
