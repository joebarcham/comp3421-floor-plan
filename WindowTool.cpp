//------------------------------------------------------------------------------
// WindowTool
//------------------------------------------------------------------------------
// Window tool for graphical editor: implements the tool for interactive
// creation of window shapes.
//------------------------------------------------------------------------------

#include <WindowTool.h>
#include <DSWindow.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// WindowTool Constructor
//------------------------------------------------------------------------------
// Init window to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

WindowTool::WindowTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init window pointer to NULL
    
    delete m_extra;
    m_name    = new QString("Window");
    m_message = new QString("Click and drag to create window");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// WindowTool Destructor
//------------------------------------------------------------------------------
// If a window was created, destroy it.
//------------------------------------------------------------------------------

WindowTool::~WindowTool(){
    if (m_p)
        delete m_p;
}

void WindowTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no window being drawn, create a new one and add this point to
//   it. The running window is added to the canvas.
// - If shift is held down while clicking, the local window is destroyed
//   (The finished window continues to reside in the canvas).
// - Otherwise just add another point to the window.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void WindowTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "WindowTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no window in progress
        m_p = new DSWindow();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add window to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this window from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    m_p->addPoint(gridPoint(evt));  // add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If window exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void WindowTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "WindowTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // window in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void WindowTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "WindowTool::mouseReleased): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    m_p = 0;
}
