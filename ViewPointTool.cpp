//------------------------------------------------------------------------------
// ViewPointTool
//------------------------------------------------------------------------------
// ViewPoint tool for graphical editor: implements the tool for interactive
// creation of viewpoint shapes.
//------------------------------------------------------------------------------

#include <ViewPointTool.h>
#include <DSViewPoint.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// ViewPointTool Constructor
//------------------------------------------------------------------------------
// Init viewpoint to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

ViewPointTool::ViewPointTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init viewpoint pointer to NULL
    
    delete m_extra;
    m_name    = new QString("ViewPoint");
    m_message = new QString("Click and drag to create viewpoint");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// ViewPointTool Destructor
//------------------------------------------------------------------------------
// If a viewpoint was created, destroy it.
//------------------------------------------------------------------------------

ViewPointTool::~ViewPointTool(){
    if (m_p)
        delete m_p;
}

void ViewPointTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no viewpoint being drawn, create a new one and add this point to
//   it. The running viewpoint is added to the canvas.
// - If shift is held down while clicking, the local viewpoint is destroyed
//   (The finished viewpoint continues to reside in the canvas).
// - Otherwise just add another point to the viewpoint.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void ViewPointTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "ViewPointTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no viewpoint in progress
        m_p = new DSViewPoint();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add viewpoint to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this viewpoint from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    m_p->addPoint(gridPoint(evt));  // add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If viewpoint exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void ViewPointTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "ViewPointTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // viewpoint in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void ViewPointTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "ViewPointTool::mouseReleased): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    m_p = 0;
}
