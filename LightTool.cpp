//------------------------------------------------------------------------------
// LightTool
//------------------------------------------------------------------------------
// Light tool for graphical editor: implements the tool for interactive
// creation of light shapes.
//------------------------------------------------------------------------------

#include <LightTool.h>
#include <DSLight.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// LightTool Constructor
//------------------------------------------------------------------------------
// Init light to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

LightTool::LightTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init light pointer to NULL
    
    delete m_extra;
    m_name    = new QString("Light");
    m_message = new QString("Click and drag to place light");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// LightTool Destructor
//------------------------------------------------------------------------------
// If a light was created, destroy it.
//------------------------------------------------------------------------------

LightTool::~LightTool(){
    if (m_p)
        delete m_p;
}

void LightTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no light being drawn, create a new one and add this point to
//   it. The running light is added to the canvas.
// - If shift is held down while clicking, the local light is destroyed
//   (The finished light continues to reside in the canvas).
// - Otherwise just add another point to the light.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void LightTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "LightTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no light in progress
        m_p = new DSLight();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add light to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this light from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }
    
    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If light exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void LightTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "LightTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // light in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}

void LightTool::mouseReleased(QMouseEvent* evt) {
    const char* methName = "LightTool::mouseReleased): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    m_p = 0;
}
