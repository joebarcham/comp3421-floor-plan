//------------------------------------------------------------------------------
// Tool
//------------------------------------------------------------------------------
// This class represents a tool such as select, translate etc in the 
// Bezier editor.
//------------------------------------------------------------------------------

#include <Tool.h>
#include <DSCanvas.h>
#include <DSMainWindow.h>

#include <QMouseEvent>
#include <QKeyEvent>
#include <QCursor>
#include <QPoint>

//------------------------------------------------------------------------------
// Tool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, point to NULL, the window using parent's constructor.
//------------------------------------------------------------------------------

Tool::Tool(DSMainWindow& mainWindow, DSCanvas* canvas) {
    m_mainWindow = &mainWindow;
    m_canvas = canvas;
    m_name = new QString();
    m_message = new QString();
    m_extra = new QString();
    m_cursor = new QCursor(Qt::ArrowCursor);
}


//------------------------------------------------------------------------------
// Tool Destructor
//------------------------------------------------------------------------------
// Delete the members.
//------------------------------------------------------------------------------

Tool::~Tool() {
    delete m_name;
    delete m_message;
    delete m_cursor;
    delete m_extra;
}

void Tool::cancel() {
    ;
}


//------------------------------------------------------------------------------
// getName
//------------------------------------------------------------------------------
// Return the tool's name
//------------------------------------------------------------------------------

QString& Tool::getName() const {
    return *m_name;
}


//------------------------------------------------------------------------------
// getMessage
//------------------------------------------------------------------------------
// Returns the message to be displayed in the status bar when this tool is
// selected
//------------------------------------------------------------------------------

QString& Tool::getMessage() const {
    return *m_message;
}


//------------------------------------------------------------------------------
// getCursor
//------------------------------------------------------------------------------
// Return the cursor to use when this tool is active
//------------------------------------------------------------------------------

QCursor& Tool::getCursor() const {
    return *m_cursor;
}

QString& Tool::getExtra() const {
    return *m_extra;
}


//------------------------------------------------------------------------------
// gridPoint
//------------------------------------------------------------------------------
// Return the mouse position, rounded to grid point.  Returns (0, 0) if the
// canvas has not been set
//------------------------------------------------------------------------------

QPoint Tool::gridPoint(QMouseEvent* evt){
    int gridSize = m_mainWindow->getGridSize();
    int x = ((evt->x() + m_canvas->getXOffset())/gridSize)*gridSize;
    int y = ((evt->y() + m_canvas->getYOffset())/gridSize)*gridSize;
    return QPoint(x, y);
}


//------------------------------------------------------------------------------
// getPoint
//------------------------------------------------------------------------------
// Return the mouse position.
//------------------------------------------------------------------------------
QPoint Tool::getPoint(QMouseEvent* evt){
    QPoint result = QPoint(evt->x() + m_canvas->getXOffset(),
                           evt->y() + m_canvas->getYOffset());
    return result;
}


//------------------------------------------------------------------------------
// handleMouseMoveEvent
//------------------------------------------------------------------------------
// Captures mouse movement events and sorts them into either mouse drag
// events (moving mouse with the button down) or simple mouse movement
// events. These are then passed to mouseMoved() or mouseDragged().
//------------------------------------------------------------------------------

void Tool::handleMouseMoveEvent(QMouseEvent* evt){
    const char* methName = "Tool::handleMouseMoveEvent(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    Qt::MouseButtons button = evt->buttons();
    if (button && Qt::LeftButton)          // mouse button down - dragging
        mouseDragged(evt);
    else                                   // button not down - just moving
        mouseMoved(evt);
}


//------------------------------------------------------------------------------
// handleMouseMoveEvent
//------------------------------------------------------------------------------
// Captures mouse movement events and sorts them into either mouse drag
// events (moving mouse with the button down) or simple mouse movement
// events. These are then passed to mouseMoved() or mouseDragged().
//------------------------------------------------------------------------------

void Tool::handleMousePressEvent(QMouseEvent* evt){
    const char* methName = "Tool::handleMousePressEvent(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    QMouseEvent::Type type = evt->type();
    // don't distinguish between press and click
    mouseClicked(evt);
    mousePressed(evt);
}

void Tool::handleMouseReleaseEvent(QMouseEvent* evt) {
    if (!m_canvas)
        return;
    mouseReleased(evt);
}

void Tool::handleKeyPressEvent(QKeyEvent* /* evt */) {
    ;
}

void Tool::handleKeyReleaseEvent(QKeyEvent* /* evt */) {
    ;
}

// empty methods
void Tool::mousePressed(QMouseEvent* /* evt */) {
    ;
}

void Tool::mouseClicked(QMouseEvent* /* evt */) {
    ;
}

void Tool::mouseReleased(QMouseEvent* /* evt */) {
    ;
}

void Tool::mouseEntered(QMouseEvent* /* evt */) {
    ;
}

void Tool::mouseExited(QMouseEvent* /* evt */) {
    ;
}

void Tool::mouseDragged(QMouseEvent* /* evt */) {
    ;    
}

void Tool::mouseMoved(QMouseEvent* /* evt */) {
    ;
}
