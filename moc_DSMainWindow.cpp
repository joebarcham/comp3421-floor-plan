/****************************************************************************
** Meta object code from reading C++ file 'DSMainWindow.h'
**
** Created: Thu Aug 23 22:42:02 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "DSMainWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DSMainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DSMainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      40,   13,   13,   13, 0x08,
      50,   13,   13,   13, 0x08,
      61,   13,   13,   13, 0x08,
      72,   13,   13,   13, 0x08,
      81,   13,   13,   13, 0x08,
      93,   13,   13,   13, 0x08,
      99,   13,   13,   13, 0x08,
     106,   13,   13,   13, 0x08,
     114,   13,   13,   13, 0x08,
     124,   13,   13,   13, 0x08,
     133,   13,   13,   13, 0x08,
     156,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DSMainWindow[] = {
    "DSMainWindow\0\0name\0toolChanged(QString)\0"
    "newFile()\0openFile()\0saveFile()\0"
    "saveAs()\0closeFile()\0cut()\0copy()\0"
    "paste()\0toFront()\0toBack()\0"
    "showPropertiesDialog()\0showAbout()\0"
};

void DSMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DSMainWindow *_t = static_cast<DSMainWindow *>(_o);
        switch (_id) {
        case 0: _t->toolChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->newFile(); break;
        case 2: _t->openFile(); break;
        case 3: _t->saveFile(); break;
        case 4: _t->saveAs(); break;
        case 5: _t->closeFile(); break;
        case 6: _t->cut(); break;
        case 7: _t->copy(); break;
        case 8: _t->paste(); break;
        case 9: _t->toFront(); break;
        case 10: _t->toBack(); break;
        case 11: _t->showPropertiesDialog(); break;
        case 12: _t->showAbout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DSMainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DSMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_DSMainWindow,
      qt_meta_data_DSMainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DSMainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DSMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DSMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DSMainWindow))
        return static_cast<void*>(const_cast< DSMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int DSMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void DSMainWindow::toolChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
