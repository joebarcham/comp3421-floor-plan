#include <Point2D.h>
#include <Edge2D.h>
#include <Matrix2D.h>
#include <QPoint>
#include <math.h>

Edge2D::Edge2D(const Point2D& start, const Point2D& end) {
    m_start = Point2D(start);
    m_end = Point2D(end);
    init();
}

Edge2D::Edge2D(int x1, int y1, int x2, int y2) {
    m_start = Point2D(x1, y1);
    m_end = Point2D(x2, y2);
    init();
}

Edge2D::~Edge2D() {
}

QString Edge2D::toString() const {
    return QString("%1 - %2").arg(m_start.toString()).arg(m_end.toString());
}

Point2D Edge2D::toLineSpace(const QPoint& p) {
    return m_toLineSpaceT.apply(Point2D(p));
}

void Edge2D::init() {
    double dx = m_end.x() - m_start.x();
    double dy = m_end.y() - m_start.y();
    double length = sqrt(dx*dx + dy*dy);
    Matrix2D m1(1,0, -m_start.x(), 0, 1, -m_start.y());
    Matrix2D m2(dx, dy, 0, -dy, dx, 0);
    Matrix2D m3(1/(length*length), 0, 0, 0 , 1/length, 0);
    m_toLineSpaceT = m1.compose(m2).compose(m3);
} 
