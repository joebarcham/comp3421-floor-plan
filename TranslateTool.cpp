//------------------------------------------------------------------------------
// TranslateTool
//------------------------------------------------------------------------------
// Translate tool for image map editor
//------------------------------------------------------------------------------

#include <TranslateTool.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>

#include <QCursor>
#include <QString>
#include <QMouseEvent>


//------------------------------------------------------------------------------
// TranslateTool Constructor
//------------------------------------------------------------------------------
// Init shape to NULL, point to NULL, the window using parent's constructor.
//------------------------------------------------------------------------------

TranslateTool::TranslateTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    :Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_shapeSelected = 0;          // init shape pointer to NULL
    m_lastPosition  = QPoint(0, 0);

    // delete default cursor
    delete m_cursor;

    // create new name, message and cursor
    m_name    = new QString("Translate");
    m_message = new QString("Click and drag an object to move it");
    m_cursor  = new QCursor(Qt::SizeAllCursor);
}


//------------------------------------------------------------------------------
// TranslateTool Destructor
//------------------------------------------------------------------------------
// If a point was allocated, delete it. Shape pointer never gets allocated, no
// need to delete.
//------------------------------------------------------------------------------

TranslateTool::~TranslateTool(){
}


//------------------------------------------------------------------------------
// mousePressed
//------------------------------------------------------------------------------
// On button down, highlight the region, and display a message.
//------------------------------------------------------------------------------
void TranslateTool::mousePressed(QMouseEvent* evt) {
    const char* methName = "TranslateTool::mousePressed(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    // ask the canvas to find a shape under the cursor
    m_shapeSelected = m_canvas->getShapes().findShape(getPoint(evt)); 
    if (!m_shapeSelected)
        return;  // there was no shape where user clicked

    m_canvas->select(m_shapeSelected); // select shape
    m_lastPosition = gridPoint(evt);   // and get a new position
    m_canvas->repaint();               // refresh canvas
}


//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// Drag selected object around
//------------------------------------------------------------------------------
void TranslateTool::mouseDragged(QMouseEvent* evt) {
    const char* methName = "TranslateTool::mouseDragged():";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_shapeSelected) {
        if (debug)
            printf("%s no selected shape\n", methName);

        return;
    }

    QPoint newPos = gridPoint(evt);// get point under cursor
    // move shape to point under cursor
    m_shapeSelected->translate(newPos.x() - m_lastPosition.x(),
                               newPos.y() - m_lastPosition.y());
    m_lastPosition = newPos;       // and get a new one
    m_canvas->modified();          // tell the canvas the file has been modified
    m_canvas->repaint();
}

