#include <DSPortal.h>
#include <Edge2D.h>
#include <Matrix2D.h>
#include <Point2D.h>
#include <StringTokenizer.h>
#include <Exceptions.h>

#include <QPolygon>
#include <QPoint>
#include <QVector>
#include <QRegion>
#include <QtOpenGL>

#include "DSCanvas.h"

#include <iostream>
#include <math.h>

using namespace std;


DSPortal::DSPortal() : DSShape() {
    m_endPoints = new QVector<Point2D>();
    m_selection = 0;
    m_name = "Portal";
}

DSPortal::DSPortal(DSPortal& p) : DSShape(p) {
    m_selection = p.m_selection;
    m_endPoints = new QVector<Point2D>();
    for(int i = 0; i < p.m_endPoints->size(); i++)
        addPoint(p.m_endPoints->at(i));
    m_fill = p.m_fill;
}

DSPortal::~DSPortal() {
    delete m_endPoints;
}

void DSPortal::paint() {
    setGLColour();
    glDisable(GL_TEXTURE_2D);
    glLineWidth(line_width);

    if (m_endPoints->size() > 0) {
        glBegin(GL_LINES);  //draw the portal 
        for (int i = 0; i < m_endPoints->size(); i++) {
            Point2D p = m_endPoints->at(i);
            glVertex2d(p.x(), p.y());
        }
        glEnd(); 
        // Dashed line
        if (m_endPoints->size() == 4) {
            glLineWidth(1.0);
            Point2D a = midpoint(m_endPoints->at(0), m_endPoints->at(1));
            Point2D b = midpoint(m_endPoints->at(2), m_endPoints->at(3));
            double t = 0;
            glBegin(GL_LINES);
            while (t < 1) {
                Point2D p1 = a.interp(b, t);
                glVertex2d(p1.x(), p1.y());
                t += dash_length / a.dist(b);

                Point2D p2 = a.interp(b, t);
                glVertex2d(p2.x(), p2.y());
                t += dash_length / a.dist(b);
            }
            glEnd();
        }
    }
}

void DSPortal::paintSelect() {
    for(int i = 0; i < m_endPoints->size(); i++){
        Point2D p = m_endPoints->at(i);
        drawPoint(p.x(), p.y());
    }
}

bool DSPortal::contains(int x, int y) {
    return (selection_dist >= Point2D::distToSeg(m_endPoints->at(0), m_endPoints->at(1), Point2D(x, y)))
     || (selection_dist >= Point2D::distToSeg(m_endPoints->at(2), m_endPoints->at(3), Point2D(x, y)));
}

void DSPortal::rotate(const Point2D& fixed, double angle) {
    const Matrix2D matrix = Matrix2D::rotateAbout(fixed, angle);
    apply(matrix);
}

void DSPortal::translate(int deltax, int deltay) {
    const Matrix2D matrix = Matrix2D(1, 0, deltax, 0, 1, deltay);
    apply(matrix);
}

void DSPortal::scale(double xFactor, double yFactor) {
    const Matrix2D matrix = Matrix2D(xFactor, 0, 0, 0, yFactor, 0);
    apply(matrix);
}

void DSPortal::addPoint(Point2D p) {
    addPoint(p.toPoint());
}

void DSPortal::addPoint(QPoint p) {
    if (m_endPoints->size() < 4) {
        m_endPoints->append(p);
        m_selection = m_endPoints->size() - 1;
    }
}


void DSPortal::setPoint(QPoint p) {
    if (m_selection >= 0) {
        m_endPoints->replace(m_selection, p);
    }
}

QPoint DSPortal::selectPoint(const QPoint& p) {
    double mind = 0;
    m_selection = -1;
    for (int i = 0; i < m_endPoints->size(); i++) {
        Point2D p2 = m_endPoints->at(i);
        double d = sqr(p2.x() - p.x()) + sqr(p2.y() - p.y());
        if ((d < mind || m_selection == -1) && d < EPSILON*EPSILON) {
            mind = d;
            m_selection = i;
        }
    }
    if (m_selection == -1) {
        return QPoint(-1, -1);
    }
    QPoint result = m_endPoints->at(m_selection).toPoint();
    return result;
}

void DSPortal::removePoint() {
    if (m_selection >= 0) {
        m_endPoints->remove(m_selection);
    }
}

Point2D DSPortal::centre() {
    Point2D a = midpoint(m_endPoints->at(0), m_endPoints->at(1));
    if (m_endPoints->size() == 4) {
        Point2D b = midpoint(m_endPoints->at(2), m_endPoints->at(3));
        return midpoint(a, b);
    } else {
        return a;
    }
}

void DSPortal::apply(const Matrix2D& t){
    for (int i = 0; i < m_endPoints->size(); i++){
        const Point2D p = t.apply(m_endPoints->at(i));
        m_endPoints->replace(i, p);
    }
}

QString DSPortal::toString() {
    QString result = DSShape::toString();

    result += " " + QString::number(m_endPoints->size());
    for (int i = 0; i < m_endPoints->size(); i++) {
        Point2D p = m_endPoints->at(i);
        result += " ";
        result += p.toString();
    }

    return result;
}

void DSPortal::fromTokens(StringTokenizer& st) throw(MalformedShapeException) {
    try {
        bool ok;
        m_selection = -1;
        int n = (st.nextToken()).toInt(&ok);  // how many points
        if ( !ok ) {
                throw NumberFormatException();
        }
        for (int i = 0; i < n; i++){
            double x = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            double y = (st.nextToken()).toDouble(&ok);
            if ( !ok ) {
                throw NumberFormatException();
            }
            addPoint(Point2D(x, y));
        }
    }
    catch (NoSuchElementException e) {
        throw (MalformedShapeException("NoSuchElementException " +
                    e.getMessage()));
    }
    catch (NumberFormatException e) {
        throw (MalformedShapeException("NumberFormatException " + 
                    e.getMessage()));
    }
}

// QPolygon DSPortal::toPolygon(const QVector<Point2D>& ps) {
//     QPolygon result = QPolygon(ps.size());
//     for (int i = 0; i < ps.size(); i++) {
//         QPoint p = ps.at(i).toPoint();
//         result.setPoint(i, p);
//     }
//     return result;
// }

double DSPortal::sqr(double x) {
    return x*x;
}

Point2D DSPortal::midpoint(Point2D a, Point2D b) {
    return Point2D((a.x()+b.x())/2, (a.y()+b.y())/2);
}

DSShape* DSPortal::clone() {
    return new DSPortal(*this);
}
