#ifndef SELECTTOOL_H
#define SELECTTOOL_H

#include <Tool.h>

class DSMainWindow;
class DSShape;
class QCursor;
class QMouseEvent;


/*!
 * Selection tool for graphical editor: selects the appropriate shape when it is 
 * clicked. Inherits a lot of common functions from Tool.
 */
class SelectTool : public Tool {
public:
    /*!
     * SelectTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    SelectTool(DSMainWindow &mainWindow, DSCanvas* canvas);
    ~SelectTool();

protected:
    /*!
     * Handles mouse click events - selects the shape under the cursor when the
     * mouse was clicked.
     */
    void mouseClicked(QMouseEvent* evt);

private:
    DSShape* m_shapeSelected; // Pointer to the currently selected shape.
};

#endif // #ifndef SELECTTOOL_H
