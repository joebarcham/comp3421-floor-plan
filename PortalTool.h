#ifndef PORTALTOOL_H
#define PORTALTOOL_H

#include <Tool.h>

class DSPortal;
class DSMainWindow;
class QMouseEvent;

/*!
 * Portal tool for graphical editor: implements the tool for interactive
 * creation of portal shapes. All the useful stuff is inherited from the
 * Tool abstract class.
 */
class PortalTool : public Tool {
public:
    /*!
     * PortalTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    PortalTool(DSMainWindow& mainWindow, DSCanvas* canvas);
    /*!
     * Free all memory being occupied by this object
     */
    ~PortalTool();
    /*!
     * Deletes the portal currently being drawn
     */
    void cancel();
protected:
    /*!
     * The portal currently being created.
     */
    DSPortal* m_p;
    int lines_drawn;

    /*!
     * Handles mouse click events - if a portal is currently being drawn, adds 
     * the point under the cursor to it, otherwise creates a new portal.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);
};


#endif // #ifndef PORTALTOOL_H
