/****************************************************************************
** Meta object code from reading C++ file 'DSCanvas.h'
**
** Created: Thu Aug 23 23:49:52 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "DSCanvas.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DSCanvas.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DSCanvas[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   10,    9,    9, 0x0a,
      40,   36,    9,    9, 0x08,
      75,   36,    9,    9, 0x08,
     111,   36,    9,    9, 0x08,
     149,   36,    9,    9, 0x08,
     181,   36,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DSCanvas[] = {
    "DSCanvas\0\0name\0toolChanged(QString)\0"
    "evt\0handleMouseMoveEvent(QMouseEvent*)\0"
    "handleMousePressEvent(QMouseEvent*)\0"
    "handleMouseReleaseEvent(QMouseEvent*)\0"
    "handleKeyPressEvent(QKeyEvent*)\0"
    "handleKeyReleaseEvent(QKeyEvent*)\0"
};

void DSCanvas::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DSCanvas *_t = static_cast<DSCanvas *>(_o);
        switch (_id) {
        case 0: _t->toolChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->handleMouseMoveEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 2: _t->handleMousePressEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 3: _t->handleMouseReleaseEvent((*reinterpret_cast< QMouseEvent*(*)>(_a[1]))); break;
        case 4: _t->handleKeyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 5: _t->handleKeyReleaseEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DSCanvas::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DSCanvas::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_DSCanvas,
      qt_meta_data_DSCanvas, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DSCanvas::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DSCanvas::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DSCanvas::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DSCanvas))
        return static_cast<void*>(const_cast< DSCanvas*>(this));
    return QWidget::qt_metacast(_clname);
}

int DSCanvas::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
