#ifndef ROTATETOOL_H
#define ROTATETOOL_H

#include <Tool.h>
#include <Point2D.h>

class DSMainWindow;
class DSShape;
class QCursor;
class QMouseEvent;


/*!
 * Selection tool for graphical editor: selects the appropriate shape when it is 
 * clicked. Inherits a lot of common functions from Tool.
 */
class RotateTool : public Tool {
public:
    /*!
     * SelectTool's constructor, needs the window and the canvas that this tool
     * is operating on.
     */
    RotateTool(DSMainWindow &mainWindow, DSCanvas* canvas);
    ~RotateTool();

protected:
    /*!
     * Handles mouse click events - selects the shape under the cursor when the
     * mouse was clicked.
     */
    void mouseClicked(QMouseEvent* evt);
    /*!
     * Handles mouse dragging events - does the same thing as for move movement.
     */
    void mouseDragged(QMouseEvent* evt);

    void mouseReleased(QMouseEvent* evt);

private:
    DSShape* m_shapeSelected; // Pointer to the currently selected shape.
    Point2D* m_centre; // Original center of shape
    double prevAngle;
};

#endif // #ifndef ROTATETOOL_H
