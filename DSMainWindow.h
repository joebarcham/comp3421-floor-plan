#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <string>
#include <map>
#include <list>

#include <QColor>
#include <QMainWindow>

using std::string;
using std::map;
using std::list;

class PropertiesDialog;
class TextureChoice;
class ToolChoice;
class DSShape;
class DSCanvas;
class Tool;

class QAction;
class QMenu;
class QWorkspace;
class QSignalMapper;
class QMouseEvent;
class QKeyEvent;
class QLabel;
class QPushButton;
class QLineEdit;
class QColorDialog;

/*!
 * This class represents the main window of the application.  It performs
 * the functionality of the DSEdit class in the original Java assignment.
 * It keeps a child window open for each file, and shares the editing panel
 * among the child windows.  It was designed with an MDI (Multiple Document
 * Interface) concept in mind
 */
class DSMainWindow : public QMainWindow {
    Q_OBJECT
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT = 600;
    map<string, string> m_configMap;
    Tool* m_currentTool;
    DSShape* m_clipboard;
    /*
     * GUI components
     */    
    // workspace
    QWorkspace* m_workspace;
    QSignalMapper *m_windowMapper;

    // file menu
    QMenu* m_fileMenu;
    QAction* m_newAction;
    QAction* m_openAction;
    QAction* m_saveAction;
    QAction* m_saveAsAction;
    QAction* m_closeAction;
    QAction* m_exitAction;
    
    // edit menu
    QMenu* m_editMenu;
    QAction* m_cutAction;
    QAction* m_copyAction;
    QAction* m_pasteAction;
    QAction* m_toFrontAction;
    QAction* m_toBackAction;
    QAction* m_propertiesAction;

    // help menu
    QMenu* m_helpMenu;
    QAction* m_aboutAction;
    
    // toolbars
    QToolBar* m_fileToolbar;
    QToolBar* m_editToolbar;    
    QToolBar* m_drawToolbar;

    // main bar
    QWidget* m_mainBar;
    ToolChoice* m_toolChoice;    
    TextureChoice* m_textureChoice;  

    // properties dialog
    PropertiesDialog* m_properties;
public:
    /*!
     * Construct a new instance of the main window.  It is expected that only of
     * of these is created per application.
     */
    DSMainWindow(); 
    /*!
     * Release all memory occupied by this object
     */
    ~DSMainWindow();
    /*!
     * Get the currently selected canvas.  Returns 0 if there are none selected
     */
    DSCanvas* getCurrentCanvas();
    /*! 
     * Assigns properties selected in the editing bar (such as colour/texture)
     * to the to the input shape
     */
    void assignProperties(DSShape& s);
    /*!
     * Returns the currently selected grid size
     */
    int getGridSize();
    /*!
     * Returns the currently selected road width
     */
    double getExtra1();
    double getExtra2();
    /*!
     * Opens a file from filename. If the file doesn't exist or is null, pops 
     * up an error box and opens a blank canvas with the given name.
     */
    void openFile(char *fileName);
    void toolChanged(const Tool& tool);
signals:
    /*!
     * This signal is emitted when the currently selected tool is changed
     */    
    void toolChanged(const QString& name);
protected:
    /*!
     * Called when this window is closed by the user
     */
    void closeEvent(QCloseEvent* event);
private:
    /*
     * Find a canvas by filename.  Returns 0 if no such canvas found
     */
    DSCanvas* findChild(const QString& filename);
    /*
     * Create the actions to be handled by this window
     */
    void createActions();
    /* 
     * Create the menus for this window -- File, Edit, Help
     */
    void createMenus();
    /*
     * Create the toolbars for this window
     */
    void createToolbars();  
    /*
     * Create the main editing bar
     */
    QWidget* createMainBar();
    /*
     * Create a new DSCanvas as a child window.  The canvas will be added to the
     * list of child windows
     */
    DSCanvas* createChildWindow();
private slots:
    // Called when File/New selected
    void newFile();
    // Called when File/Open is selected
    void openFile();
    // Called when File/Save is selected
    void saveFile();
    // Called when File/Save As is selected
    void saveAs();
    // Called when File/Close is selected
    void closeFile();
    // Called when Edit/Cut is selected
    void cut();
    // Called when Edit/Copy is selected
    void copy();
    // Called when Edit/Paste is selected
    void paste();
    // Called when Edit/Move to Front is selected
    void toFront();
    // Called when Edit/Move to Back is selected
    void toBack();
    // Called when Edit/Properties is selected
    void showPropertiesDialog();
    // Called when Help/About is selected
    void showAbout();
};

#endif
