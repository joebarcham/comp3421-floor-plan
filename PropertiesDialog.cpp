#include <PropertiesDialog.h>
#include <TextureChoice.h>
#include <DSMainWindow.h>
#include <DSShape.h>
#include <DSCanvas.h>

#include <QString>
#include <QtGui>

PropertiesDialog::PropertiesDialog(QWidget* parent) :
    QDialog(parent) {
    m_textureChoice = new TextureChoice(this);

    m_extraLabel = new QLabel("Height: ");
    m_extraValue = new QLineEdit(this);
    m_extraValidator = new QDoubleValidator(m_extraValue);
    m_extraValue->setValidator(m_extraValidator);
    m_extraValue->setFixedSize(QSize(60, m_extraValue->height()));
    m_extraValue->setText(QString("1.0"));
    m_extraValue->setMaxLength(7);

    m_propsLayout = new QHBoxLayout();
    m_propsLayout->addStretch(1);
    m_propsLayout->addWidget(m_textureChoice);
    m_propsLayout->addWidget(m_extraLabel);
    m_propsLayout->addWidget(m_extraValue);
    m_propsLayout->addStretch(1);

    m_okButton = new QPushButton("Set");
    connect(m_okButton, SIGNAL(clicked()), this, SLOT(updateProperties()));
    m_cancelButton = new QPushButton("Cancel");
    connect(m_cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    
    m_buttonsLayout = new QHBoxLayout();
    m_buttonsLayout->addStretch(1);
    m_buttonsLayout->addWidget(m_okButton);
    m_buttonsLayout->addWidget(m_cancelButton);
    m_buttonsLayout->addStretch(1);

    m_mainLayout = new QVBoxLayout();
    m_mainLayout->addLayout(m_propsLayout);
    m_mainLayout->addLayout(m_buttonsLayout);

    setWindowTitle("Change Properties?");
    setLayout(m_mainLayout);
}

PropertiesDialog::~PropertiesDialog() {
    delete m_textureChoice;
    delete m_okButton;
    delete m_cancelButton;
    delete m_buttonsLayout;
    delete m_propsLayout;
    delete m_mainLayout;
    delete m_extraValidator;
    delete m_extraLabel;
    delete m_extraValue;
}

void PropertiesDialog::setShape(DSShape& s) {
    m_currentShape = &s;
    QColor colour = s.getFillColour();
    m_textureChoice->setColour(colour);
    m_textureChoice->setTexture(s.getTexture());
    m_textureChoice->setScale(s.getScale());
    m_extraValue->setText(QString::number(s.getExtra1()));
}

void PropertiesDialog::updateProperties() {
    m_currentShape->setFillColour(m_textureChoice->getColour());
    m_currentShape->setTexture(m_textureChoice->getTexture());
    m_currentShape->setExtra1(m_extraValue->text().toDouble());
    m_currentShape->setScale(m_textureChoice->getScale());
    DSCanvas* canvas = m_currentShape->getCanvas();
    if (canvas != 0) {
        canvas->modified();
    }
    emit accept();
}
