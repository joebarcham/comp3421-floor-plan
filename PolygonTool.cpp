//------------------------------------------------------------------------------
// PolygonTool
//------------------------------------------------------------------------------
// Polygon tool for graphical editor: implements the tool for interactive
// creation of polygon shapes.
//------------------------------------------------------------------------------

#include <PolygonTool.h>
#include <DSPolygon.h>
#include <DSMainWindow.h>
#include <DSCanvas.h>

#include <QString>
#include <QMouseEvent>
#include <QKeyEvent>

//------------------------------------------------------------------------------
// PolygonTool Constructor
//------------------------------------------------------------------------------
// Init polygon to NULL, init the window & canvas using parent's constructor.
//------------------------------------------------------------------------------

PolygonTool::PolygonTool(DSMainWindow& mainWindow, DSCanvas* canvas)
    : Tool(mainWindow, canvas) {   // throw stuff to parent's constructor
    m_p = 0;                       // init polygon pointer to NULL
    
    delete m_extra;
    m_name    = new QString("Polygon");
    m_message = new QString("Click at each corner, Shift Click on last point");
    m_extra = new QString("Height: ");
}


//------------------------------------------------------------------------------
// PolygonTool Destructor
//------------------------------------------------------------------------------
// If a polygon was created, destroy it.
//------------------------------------------------------------------------------

PolygonTool::~PolygonTool(){
    if (m_p)
        delete m_p;
}

void PolygonTool::cancel() {
    if (m_p) {
        DSShapeList& list = m_canvas->getShapes();
        list.remove(m_p);
        m_canvas->select(0);
        delete m_p;
        m_canvas->repaint();
    }
    m_p = 0;
}


//------------------------------------------------------------------------------
// mouseClicked
//------------------------------------------------------------------------------
// Handle mouse clicks:
// - If there is no polygon being drawn, create a new one and add this point to
//   it. The running polygon is added to the canvas.
// - If shift is held down while clicking, the local polygon is destroyed
//   (The finished polygon continues to reside in the canvas).
// - Otherwise just add another point to the polygon.
//
// Refresh the canvas after this.
//------------------------------------------------------------------------------

void PolygonTool::mouseClicked(QMouseEvent* evt) {
    const char* methName = "PolygonTool::mouseClicked(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);

    if (!m_p) {                          // no polygon in progress
        m_p = new DSPolygon();
        m_p->setCanvas(*m_canvas);
        m_mainWindow->assignProperties(*m_p);
        m_canvas->getShapes().add(m_p); // add polygon to canvas' list of shapes
        m_canvas->modified();
        m_canvas->select(m_p);          // select this polygon from the list
        m_p->addPoint(gridPoint(evt));  // and add a point to it at cursor pos
    }

    if (evt->modifiers() & Qt::ShiftModifier) // shift presed?
        m_p = 0;                        // then stop making the polygon
    else
        m_p->addPoint(gridPoint(evt));  // otherwise add another point

    // refresh the canvas
    m_canvas->repaint();
}

//------------------------------------------------------------------------------
// mouseDragged
//------------------------------------------------------------------------------
// If polygon exists, move the current point under the mouse cursor and refresh
// the canvas.
//------------------------------------------------------------------------------

void PolygonTool::mouseDragged(QMouseEvent* evt){
    const char* methName = "PolygonTool::mouseDragged(): ";
    bool debug = false;

    if (debug)
        printf("%s called\n", methName);

    if (m_p){  // polygon in progress
        m_p->setPoint(gridPoint(evt));
        m_canvas->repaint();
    }
}


//------------------------------------------------------------------------------
// mouseMoved
//------------------------------------------------------------------------------
// Do the same thing as for dragging.
//------------------------------------------------------------------------------

void PolygonTool::mouseMoved(QMouseEvent* evt) {
    const char* methName = "PolygonTool::mouseMoved(): ";
    bool debug = false;
    if (debug)
        printf("%s called\n", methName);    

    mouseDragged(evt);
}
